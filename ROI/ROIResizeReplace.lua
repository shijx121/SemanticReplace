local ROIResizeReplace, parent = torch.class('ROIResizeReplace', 'nn.Module')
require 'image'

function ROIResizeReplace:__init(W, H, spatial_scale)
    parent.__init(self)
    self.W = W
    self.H = H
    self.spatial_scale = spatial_scale
    self.gradInput = {torch.Tensor()}
end

function ROIResizeReplace:updateOutput(input)
    -- input[1]: source image feature, 1 x channel x H x W
    -- input[2]: {{1, x1, y1, x2, y2}}, x/y is in original image and start from 1
    -- input[3]: target image feature, 1 x channel x H x W
    -- input[4]: {{1, x1, y1, x2, y2}}, x/y is in original image and start from 1
    local source = input[1]
    local target = input[3]
    assert(source:size(1)==1 and source:size(3)==self.H and source:size(4)==self.W)
    assert(target:size(1)==1 and target:size(3)==self.H and target:size(4)==self.W)
    local roisA = input[2]:clone():float()
    local roisB = input[4]:clone():float()

    local roiA = roisA[1]
    roiA[{{2,5}}]:add(-1):mul(self.spatial_scale):add(1):round()
    local x1A = math.min(roiA[2], self.W)
    local y1A = math.min(roiA[3], self.H)
    local x2A = math.min(roiA[4], self.W)
    local y2A = math.min(roiA[5], self.H)

    local roiB = roisB[1]
    roiB[{{2,5}}]:add(-1):mul(self.spatial_scale):add(1):round()
    local x1B = math.min(roiB[2], self.W)
    local y1B = math.min(roiB[3], self.H)
    local x2B = math.min(roiB[4], self.W)
    local y2B = math.min(roiB[5], self.H)

    self.output:resizeAs(source):copy(source)
    target = target:clone():squeeze():float()
    self.output[{{1},{},{y1A,y2A},{x1A,x2A}}] = image.scale(target[{{},{y1B,y2B},{x1B,x2B}}], x2A-x1A+1, y2A-y1A+1, 'bicubic')
    return self.output
end

function ROIResizeReplace:updateGradInput(input, gradOutput)
    self.gradInput[1]:resizeAs(gradOutput):zero()
    return self.gradInput
end

function ROIResizeReplace:type(type)
  parent.type(self,type)
  self._type = type
  return self
end


