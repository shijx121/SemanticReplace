local ROIUpsampling, parent = torch.class('ROIUpsampling', 'nn.Module')

function ROIUpsampling:__init(iW, iH, oW, oH, spatial_scale)
    parent.__init(self)
    self.iW = iW
    self.iH = iH
    self.oW = oW
    self.oH = oH
    self.spatial_scale = spatial_scale
    self.gradInput = {torch.Tensor()}
    self.mapcount = torch.FloatTensor()
end

function ROIUpsampling:updateOutput(input)
    -- input[1]: 1 x channel x H x W
    -- input[2]: {{1, x1, y1, x2, y2}}, x/y is in original image and start from 1
    local data = input[1]:float()
    local nc = data:size(2)
    assert(data:size(1)==1 and data:size(3)==self.iH and data:size(4)==self.iW)
    
    local rois = input[2]:clone()
    assert(rois:size(1)==1 and rois:size(2)==5)

    -- element access is faster if not a cuda tensor
    if rois:type() == 'torch.CudaTensor' then
        self._rois = self._rois or torch.FloatTensor()
        self._rois:resize(rois:size()):copy(rois)
        rois = self._rois
    end

    local roi = rois[1]
    roi[{{2,5}}]:add(-1):mul(self.spatial_scale):add(1):round()
    roi[2] = math.min(roi[2], self.oW)
    roi[3] = math.min(roi[3], self.oH)
    roi[4] = math.min(roi[4], self.oW)
    roi[5] = math.min(roi[5], self.oH)

    if not self._type then self._type = self.output:type() end

    self.output = self.output:resize(1, nc, self.oH, self.oW)
    self.output = self.output:float()
    self.mapcount = self.mapcount:float():resize(1, nc, self.oH, self.oW):zero()
    
    local x_j_start, x_j_end, y_i_start, y_i_end, roi_w, roi_h, x1, y1, x2, y2
    self.output[1]:zero()
    x1, y1, x2, y2 = roi[2], roi[3], roi[4], roi[5]
    roi_h = y2 - y1 + 1
    roi_w = x2 - x1 + 1
    for i=1,self.iH do
        for j=1,self.iW do
            x_j_start = math.floor((j-1) * roi_w / self.iW + 1) 
            -- start from 1
            x_j_end   = math.ceil(j * roi_w / self.iW)

            y_i_start = math.floor((i-1) * roi_h / self.iH + 1)
            y_i_end   = math.ceil(i * roi_h / self.iH)
            self.output[{{1},{},{y1+y_i_start-1, y1+y_i_end-1}, {x1+x_j_start-1, x1+x_j_end-1}}]:add(torch.repeatTensor(data[{{1},{},{i},{j}}], 1, 1, y_i_end-y_i_start+1, x_j_end-x_j_start+1)) -- repeat to match size
            self.mapcount[{{1},{},{y1+y_i_start-1, y1+y_i_end-1}, {x1+x_j_start-1, x1+x_j_end-1}}]:add(1)
        end
    end
    self.output[{{1},{},{y1,y2},{x1,x2}}]:cdiv(self.mapcount[{{1},{},{y1,y2},{x1,x2}}])
    self.output = self.output:cuda()
    return self.output
end
    
function ROIUpsampling:updateGradInput(input,gradOutput)
    if not gradOutput then
        self.gradInput[1]:resizeAs(input[1]):zero()
        return self.gradInput
    end
    local data = input[1]:float()
    local rois = input[2]:clone()
    if rois:type() == 'torch.CudaTensor' then
        rois = self._rois
    end
    local roi = rois[1]

    self.gradInput[1] = self.gradInput[1]:float()
    self.gradInput[1]:resizeAs(data):zero()
    local x_j_start, x_j_end, y_i_start, y_i_end, roi_w, roi_h, x1, y1
    self.output[1]:zero()
    roi_w = roi[4] - roi[2] + 1
    roi_h = roi[5] - roi[3] + 1
    x1, y1 = roi[2], roi[3]
    for i=1,self.iH do
        for j=1,self.iW do
            x_j_start = math.floor((j-1) * roi_w / self.iW + 1) 
            -- start from 1, and no intersection between end_j and start_j+1 when using floor for both cases! Convenient!
            x_j_end   = math.ceil(j * roi_w / self.iW)

            y_i_start = math.floor((i-1) * roi_h / self.iH + 1)
            y_i_end   = math.ceil(i * roi_h / self.iH)
            self.gradInput[{{1},{},{i},{j}}] = torch.mean(torch.mean(gradOutput[{{1},{},{y1+y_i_start-1, y1+y_i_end-1}, {x1+x_j_start-1, x1+x_j_end-1}}], 3), 4)  -- average at H dim and W dim
        end
    end
    self.gradInput[1] = self.gradInput[1]:cuda()
    return self.gradInput
end
        
function ROIUpsampling:type(type)
  parent.type(self,type)
  self._type = type
  return self
end

