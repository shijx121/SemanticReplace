local ROIReplace, parent = torch.class('ROIReplace', 'nn.Module')

function ROIReplace:__init(W, H, spatial_scale)
    parent.__init(self)
    self.W = W
    self.H = H
    self.spatial_scale = spatial_scale
    self.gradInput = {torch.Tensor()}
end

function ROIReplace:updateOutput(input)
    -- input[1]: source image feature, 1 x channel x H x W
    -- input[2]: target image feature, 1 x channel x H x W
    -- input[3]: {{1, x1, y1, x2, y2}}, x/y is in original image and start from 1
    local source = input[1]
    local target = input[2]
    assert(source:size(1)==1 and source:size(3)==self.H and source:size(4)==self.W)
    assert(target:size(1)==1 and target:size(3)==self.H and target:size(4)==self.W)
    local rois = input[3]:clone()
    assert(rois:size(1)==1 and rois:size(2)==5)

    -- element access is faster if not a cuda tensor
    if rois:type() == 'torch.CudaTensor' then
        self._rois = self._rois or torch.FloatTensor()
        self._rois:resize(rois:size()):copy(rois)
        rois = self._rois
    end
    if not self._type then self._type = self.output:type() end
    
    local roi = rois[1]
    roi[{{2,5}}]:add(-1):mul(self.spatial_scale):add(1):round()
    roi[2] = math.min(roi[2], self.W)
    roi[3] = math.min(roi[3], self.H)
    roi[4] = math.min(roi[4], self.W)
    roi[5] = math.min(roi[5], self.H)
    local x1, y1, x2, y2 = roi[2], roi[3], roi[4], roi[5]

    self.output:resizeAs(source):copy(source)
    self.output[{{1},{},{y1,y2},{x1,x2}}] = target[{{1},{},{y1,y2},{x1,x2}}]
    return self.output
end

function ROIReplace:updateGradInput(input, gradOutput)
    local rois = input[3]:clone()
    if rois:type() == 'torch.CudaTensor' then
        rois = self._rois
    end
    local roi = rois[1]
    local x1, y1, x2, y2 = roi[2], roi[3], roi[4], roi[5]

    self.gradInput[1]:resizeAs(gradOutput):copy(gradOutput)
    self.gradInput[1][{{1},{},{y1,y2},{x1,x2}}] = 0
    return self.gradInput
end

function ROIReplace:type(type)
  parent.type(self,type)
  self._type = type
  return self
end


