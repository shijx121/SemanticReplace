require 'torch'
require 'nn'
require 'cunn'
require 'cudnn'
require 'detection'
json = require 'cjson'

opt = {
    cocoDir = '/data/mscoco',
    subCatsJson = './data/cocoObj/subCats.json',
    featNetModelPath = './FRCNN.lua',
    featNetWeightPath = './models/frcnn_vgg16_mscoco_iter_100000.t7',
    nfeat = 128,
    savePath = './data/cocoObj/featNorm.json',
    gpu = 1,
}
cutorch.setDevice(opt.gpu)
paths.dofile('./data/cocoObj/donkey_folder.lua')

print(trainLoader)

local netFeat = dofile(opt.featNetModelPath)(opt) 
local loaded = torch.load(opt.featNetWeightPath)
local model_modules = netFeat:listModules()
local loaded_modules = loaded:listModules()
for i=1, #loaded_modules do
    local copy_name = loaded_modules[i].name
    if copy_name then
        for j=1, #model_modules do
            local my_name = model_modules[j].name
            if my_name and my_name == copy_name then
                print('Copying weights from ' .. my_name .. ' layer!')
                model_modules[j].weight:copy(loaded_modules[i].weight)
                model_modules[j].bias:copy(loaded_modules[i].bias)
            end
        end
    end
end

netFeat:evaluate()
netFeat:cuda()
print('feature net loaded.')

function prep4feat(im)
    -- input: rgb, [-1,1], 3xhxw. Note im has no batch dimension!
    -- output: bgr, [0,255] and subtract mean
    local im_out = im:clone()
    im_out[1] = im[3]; im_out[3] = im[1];
    im_out:add(1):mul(0.5*255)
    im_out[1]:add(-102.9801)
    im_out[2]:add(-115.9465)
    im_out[3]:add(-122.7717)
    return im_out
end




local Ex = torch.FloatTensor(opt.nfeat):fill(0)
local Ex2 = torch.FloatTensor(opt.nfeat):fill(0)
local var
for i=1, trainLoader:size() do
    if i%1000 == 0 then
        print(i..'/'..trainLoader:size())
    end
    local im, _, __, x1, y1, x2, y2 = trainLoader:sample(i)
    im = prep4feat(im):view(1, im:size(1), im:size(2), im:size(3)):cuda()
    local bbox = torch.Tensor({{1, x1, y1, x2, y2}}):cuda()
    local f = netFeat:forward({im, bbox}):float()
    Ex:add(f)
    Ex2:add(torch.pow(f, 2))
end
Ex:mul(1/trainLoader:size())
Ex2:mul(1/trainLoader:size())

var = Ex2:clone()
var:csub(torch.pow(Ex, 2))

print(Ex)
print(var)

j=json.encode({mean=torch.totable(Ex), var=torch.totable(var)})
print(j)
io.open(opt.savePath, 'w'):write(j)
    
