--[[
    This data loader is a modified version of the one from dcgan.torch
    (see https://github.com/soumith/dcgan.torch/blob/master/data/donkey_folder.lua).

    Copyright (c) 2016, Deepak Pathak [See LICENSE file for details]

    Copyright (c) 2015-present, Facebook, Inc.
    All rights reserved.
    This source code is licensed under the BSD-style license found in the
    LICENSE file in the root directory of this source tree. An additional grant
    of patent rights can be found in the PATENTS file in the same directory.
]]--

-------------------------------------------------------------------------------------------
-- This file contains the data-loading logic and details. It is run by each data-loader thread.
-------------------------------------------------------------------------------------------

require 'image'
require 'torch'
torch.setdefaulttensortype('torch.FloatTensor')
local ffi = require 'ffi'
local class = require('pl.class')
local dir = require 'pl.dir'
local tablex = require 'pl.tablex'
local argcheck = require 'argcheck'
require 'sys'
require 'xlua'
require 'pl'



-------------------------------------------------------------------------------------------
-- list all image file names in the given folder
-------------------------------------------------------------------------------------------
local function listImages(path)
   local extensionList = {'jpg', 'png','JPG','PNG','JPEG', 'ppm', 'PPM', 'bmp', 'BMP'}
   local findOptions = ' -iname "*.' .. extensionList[1] .. '"'
   for i=2,#extensionList do
      findOptions = findOptions .. ' -o -iname "*.' .. extensionList[i] .. '"'
   end
   local imagePaths = torch.CharTensor()  -- path to each image
   local imageList = os.tmpname();

   local command = 'find -H "' .. path .. '" ' .. findOptions .. ' >>"' .. imageList .. '" \n'
   os.execute(command)

   local maxPathLength = tonumber(sys.fexecute("wc -L '" .. imageList .. "' | cut -f1 -d' '")) + 1
   local length = tonumber(sys.fexecute("wc -l '" .. imageList .. "' | cut -f1 -d' '"))
   assert(length > 0, "Could not find any image file in the given input path")
   assert(maxPathLength > 0, "paths of files are length 0?")
   imagePaths:resize(length, maxPathLength):fill(0)
   local s_data = imagePaths:data()
   local count = 0
   for line in io.lines(imageList) do
      ffi.copy(s_data, line)
      s_data = s_data + maxPathLength
      count = count + 1
   end
   print(string.format('list %d images from %s', count, path))
   os.execute('rm -f "' .. imageList .. '"')
   return imagePaths 
end


-------------------------------------------------------------------------------------------
-- function to load the real image and background image
-- include: resize, crop, flip, range
-------------------------------------------------------------------------------------------
local imageLoad = function(path)
   collectgarbage()
   local input = image.load(path, opt.nc, 'float')
   -- find the smaller dimension, and resize it to opt.loadSize (while keeping aspect ratio)
   if opt.loadSize>0 then
     local iW = input:size(3)
     local iH = input:size(2)
     if iW < iH then
        input = image.scale(input, opt.loadSize, opt.loadSize * iH / iW)
     else
        input = image.scale(input, opt.loadSize * iW / iH, opt.loadSize)
     end
   else
     local scalef = 0
     if opt.loadSize == -1 then
       scalef = torch.uniform(0.5,1.5)
     else
       scalef = torch.uniform(1,3)
     end
     local iW = scalef*input:size(3)
     local iH = scalef*input:size(2)
     input = image.scale(input, iH, iW)
   end
   local iW = input:size(3)
   local iH = input:size(2)

   -- do random crop to opt.fineSize
   local oW = opt.fineSize
   local oH = opt.fineSize
   local h1 = math.ceil(torch.uniform(1e-2, iH-oH))
   local w1 = math.ceil(torch.uniform(1e-2, iW-oW))
   local out = image.crop(input, w1, h1, w1 + oW, h1 + oH)
   assert(out:size(2) == oW)
   assert(out:size(3) == oH)
   
   -- do hflip with probability 0.5
   if torch.uniform() > 0.5 then out = image.hflip(out); end
   -- make pixel value range [0, 1] -> [-1, 1]
   out:mul(2):add(-1)
   return out
end

-------------------------------------------------------------------------------------------
-- function to load mask and mask object
-- randomly expand to fineSize x fineSize
-------------------------------------------------------------------------------------------
local maskLoad = function(maskpath, objpath)
    collectgarbage()
    local mask = image.load(maskpath, 1, 'float'):round():byte():squeeze()
    local obj = image.load(objpath, 3, 'float')
    assert(mask:size(1)==obj:size(2) and mask:size(2)==obj:size(3))
    local iW = mask:size(2)
    local iH = mask:size(1)
    local oW = opt.fineSize
    local oH = opt.fineSize
    local h1 = math.ceil(torch.uniform(1e-2, oH-iH))
    local w1 = math.ceil(torch.uniform(1e-2, oW-iW))
    local obj_out = torch.Tensor(3, oH, oW):fill(0)
    local mask_out = torch.ByteTensor(oH, oW):fill(0)
    mask_out[{{h1, h1+iH-1}, {w1, w1+iW-1}}] = mask
    obj_out[{{}, {h1, h1+iH-1}, {w1, w1+iW-1}}] = obj
    if torch.uniform() > 0.5 then 
        mask_out = image.hflip(mask_out)
        obj_out = image.hflip(obj_out)
    end
    obj_out:mul(2):add(-1)
    -- mask_out value 0/1, 1 for mask. obj_out range [-1, 1]
    return mask_out, obj_out
end



-------------------------------------------------------------------------------------------
-- class dataLoader.
-------------------------------------------------------------------------------------------
local dataset = torch.class('dataLoader')

function dataset:__init(opt)
   for k,v in pairs(opt) do self[k] = v end
   
   self.imagePaths = listImages(self.imagePath)
   self.bgPaths = listImages(self.bgPath)
   self.maskPaths = listImages(self.maskPath)
   self.imageNum = self.imagePaths:size(1)
end

function dataset:size()
    return self.imageNum
end

function dataset:sample(quantity)
   assert(quantity)
   local image = torch.Tensor(quantity, self.nc, self.fineSize, self.fineSize)  -- (batch, channel, h, w)
   local bg = torch.Tensor(quantity, self.nc, self.fineSize, self.fineSize)     -- (batch, channel, h, w)
   local maskobj = torch.Tensor(quantity, self.nc, self.fineSize, self.fineSize) -- (batch, channel, h, w)
   local mask = torch.ByteTensor(quantity, self.fineSize, self.fineSize) -- (batch, h, w)
   local imageNum, bgNum, maskNum = self.imagePaths:size(1), self.bgPaths:size(1), self.maskPaths:size(1)
   for i=1,quantity do
      local imagepath = ffi.string(torch.data(self.imagePaths[torch.random(1, imageNum)]))
      local bgpath = ffi.string(torch.data(self.bgPaths[torch.random(1, bgNum)]))
      local maskpath = ffi.string(torch.data(self.maskPaths[torch.random(1, maskNum)]))
      local maskobjpath = path.join(self.maskObjPath, path.basename(maskpath))
      image[i]:copy(imageLoad(imagepath))
      bg[i]:copy(imageLoad(bgpath))
      _, __ = maskLoad(maskpath, maskobjpath)
      mask[i]:copy(_)
      maskobj[i]:copy(__)
   end
   return image, bg, mask, maskobj
end
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------
-- trainLoader
-------------------------------------------------------------------------------------------
trainLoader = dataLoader(opt)
collectgarbage()

