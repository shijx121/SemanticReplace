--[[
    This data loader is a modified version of the one from dcgan.torch
    (see https://github.com/soumith/dcgan.torch/blob/master/data/donkey_folder.lua).

    Copyright (c) 2016, Deepak Pathak [See LICENSE file for details]

    Copyright (c) 2015-present, Facebook, Inc.
    All rights reserved.
    This source code is licensed under the BSD-style license found in the
    LICENSE file in the root directory of this source tree. An additional grant
    of patent rights can be found in the PATENTS file in the same directory.
]]--

require 'image'
paths.dofile('dataset.lua')

-- This file contains the data-loading logic and details.
-- It is run by each data-loader thread.
------------------------------------------
if not paths.dirp(opt.imagePath) or not paths.dirp(opt.maskPath) then
    error('Did not find path')
end

--------------------------------------------------------------------------------------------
local nc = opt.nc
local loadSize   = {nc, opt.loadSize}
local sampleSize = {nc, opt.fineSize}

local function loadImage(path)
   local input = image.load(path, nc, 'float')
   -- find the smaller dimension, and resize it to loadSize[2] (while keeping aspect ratio)
   if loadSize[2]>0 then
     local iW = input:size(3)
     local iH = input:size(2)
     if iW < iH then
        input = image.scale(input, loadSize[2], loadSize[2] * iH / iW)
     else
        input = image.scale(input, loadSize[2] * iW / iH, loadSize[2])
     end
   elseif loadSize[2]<0 then
    local scalef = 0
     if loadSize[2] == -1 then
       scalef = torch.uniform(0.5,1.5)
     else
       scalef = torch.uniform(1,3)
     end
     local iW = scalef*input:size(3)
     local iH = scalef*input:size(2)
     input = image.scale(input, iH, iW)
   end
   return input
end

-- channel-wise mean and std. Calculate or load them from disk later in the script.
local mean,std
--------------------------------------------------------------------------------
-- Hooks that are used for each image that is loaded

-- function to load the image, jitter it appropriately (random crops etc.)
local imageHook = function(self, path)
   collectgarbage()
   local input = loadImage(path)
   local iW = input:size(3)
   local iH = input:size(2)

   -- do random crop
   local oW = sampleSize[2];
   local oH = sampleSize[2]
   local h1 = math.ceil(torch.uniform(1e-2, iH-oH))
   local w1 = math.ceil(torch.uniform(1e-2, iW-oW))
   local out = image.crop(input, w1, h1, w1 + oW, h1 + oH)
   assert(out:size(2) == oW)
   assert(out:size(3) == oH)
   -- do hflip with probability 0.5
   if torch.uniform() > 0.5 then out = image.hflip(out); end
   out:mul(2):add(-1) -- make it [0, 1] -> [-1, 1]
   return out
end

local maskHook = function(self, path)
    collectgarbage()
    local input = image.load(path, 1, 'float'):round():byte():squeeze() -- value 0/1, 0 for background and 1 for mask
    local iW = input:size(2)
    local iH = input:size(1)
    local oW = sampleSize[2]
    local oH = sampleSize[2]
    local h1 = math.ceil(torch.uniform(1e-2, oH-iH))
    local w1 = math.ceil(torch.uniform(1e-2, oW-iW))
    local out = torch.ByteTensor(oH, oW):fill(0)
    out[{{h1, h1+iH-1}, {w1, w1+iW-1}}] = input
    if torch.uniform() > 0.5 then out = image.hflip(out); end
    return out
end

--------------------------------------
-- trainLoader
print('Creating train metadata')
trainLoader = dataLoader{
  imagePath = opt.imagePath,
  maskPath = opt.maskPath,
  loadSize = {nc, loadSize[2], loadSize[2]},
  sampleSize = {nc, sampleSize[2], sampleSize[2]},
  verbose = true
}
trainLoader.sampleHookImage = imageHook
trainLoader.sampleHookMask = maskHook
collectgarbage()

