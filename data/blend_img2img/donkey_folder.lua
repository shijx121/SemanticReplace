--[[
    This data loader is a modified version of the one from dcgan.torch
    (see https://github.com/soumith/dcgan.torch/blob/master/data/donkey_folder.lua).

    Copyright (c) 2016, Deepak Pathak [See LICENSE file for details]

    Copyright (c) 2015-present, Facebook, Inc.
    All rights reserved.
    This source code is licensed under the BSD-style license found in the
    LICENSE file in the root directory of this source tree. An additional grant
    of patent rights can be found in the PATENTS file in the same directory.
]]--

-------------------------------------------------------------------------------------------
-- This file contains the data-loading logic and details. It is run by each data-loader thread.
-------------------------------------------------------------------------------------------

require 'image'
require 'torch'
torch.setdefaulttensortype('torch.FloatTensor')
local ffi = require 'ffi'
local class = require('pl.class')
local dir = require 'pl.dir'
local tablex = require 'pl.tablex'
local argcheck = require 'argcheck'
require 'sys'
require 'xlua'
require 'pl'



-------------------------------------------------------------------------------------------
-- list all image file names in the given folder
-------------------------------------------------------------------------------------------
local function listImages(path)
   local extensionList = {'jpg', 'png','JPG','PNG','JPEG', 'ppm', 'PPM', 'bmp', 'BMP'}
   local findOptions = ' -iname "*.' .. extensionList[1] .. '"'
   for i=2,#extensionList do
      findOptions = findOptions .. ' -o -iname "*.' .. extensionList[i] .. '"'
   end
   local imagePaths = torch.CharTensor()  -- path to each image
   local imageList = os.tmpname();

   local command = 'find -H "' .. path .. '" ' .. findOptions .. ' >>"' .. imageList .. '" \n'
   os.execute(command)

   local maxPathLength = tonumber(sys.fexecute("wc -L '" .. imageList .. "' | cut -f1 -d' '")) + 1
   local length = tonumber(sys.fexecute("wc -l '" .. imageList .. "' | cut -f1 -d' '"))
   assert(length > 0, "Could not find any image file in the given input path")
   assert(maxPathLength > 0, "paths of files are length 0?")
   imagePaths:resize(length, maxPathLength):fill(0)
   local s_data = imagePaths:data()
   local count = 0
   for line in io.lines(imageList) do
      ffi.copy(s_data, line)
      s_data = s_data + maxPathLength
      count = count + 1
   end
   print(string.format('list %d images from %s', count, path))
   os.execute('rm -f "' .. imageList .. '"')
   return imagePaths 
end


-------------------------------------------------------------------------------------------
-- function to load the real image and background image
-- include: resize, crop, flip, range
-------------------------------------------------------------------------------------------
local imageLoad = function(path, rand)
   collectgarbage()
   local img = image.load(path, opt.nc, 'float')
   -- do hflip with probability 0.5
   if rand > 0.5 then img = image.hflip(img); end
   -- make pixel value range [0, 1] -> [-1, 1]
   img:mul(2):add(-1)
   return img
end

-------------------------------------------------------------------------------------------
-- class dataLoader.
-------------------------------------------------------------------------------------------
local dataset = torch.class('dataLoader')

function dataset:__init(opt)
   for k,v in pairs(opt) do self[k] = v end
   
   self.srcPaths = listImages(self.srcPath)
   self.imageNum = self.srcPaths:size(1)
end

function dataset:size()
    return self.imageNum
end

function dataset:sample(quantity)
   assert(quantity)
   local src = torch.Tensor(quantity, self.nc, self.fineSize, self.fineSize)  -- (batch, channel, h, w)
   local dst = torch.Tensor(quantity, self.nc, self.fineSize, self.fineSize)     -- (batch, channel, h, w)
   for i=1,quantity do
      local filename = paths.basename(ffi.string(torch.data(self.srcPaths[torch.random(1, self.imageNum)])))
      local srcpath = path.join(self.srcPath, filename)
      local dstpath = path.join(self.dstPath, filename)
      local rand = torch.uniform()
      src[i]:copy(imageLoad(srcpath, rand))
      dst[i]:copy(imageLoad(dstpath, rand))
   end
   return src, dst
end
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------
-- trainLoader
-------------------------------------------------------------------------------------------
trainLoader = dataLoader(opt)
collectgarbage()

