--[[
    This data loader is a modified version of the one from dcgan.torch
    (see https://github.com/soumith/dcgan.torch/blob/master/data/donkey_folder.lua).

    Copyright (c) 2016, Deepak Pathak [See LICENSE file for details]

    Copyright (c) 2015-present, Facebook, Inc.
    All rights reserved.
    This source code is licensed under the BSD-style license found in the
    LICENSE file in the root directory of this source tree. An additional grant
    of patent rights can be found in the PATENTS file in the same directory.
]]--

-------------------------------------------------------------------------------------------
-- This file contains the data-loading logic and details. It is run by each data-loader thread.
-------------------------------------------------------------------------------------------

require 'image'
require 'torch'
torch.setdefaulttensortype('torch.FloatTensor')
local ffi = require 'ffi'
local class = require('pl.class')
local dir = require 'pl.dir'
local tablex = require 'pl.tablex'
local argcheck = require 'argcheck'
require 'sys'
require 'xlua'
require 'pl'
coco = require 'coco'
json = require 'cjson'

-------------------------------------------------------------------------------------------
-- class dataLoader.
-------------------------------------------------------------------------------------------
local dataset = torch.class('dataLoader')

function dataset:__init(opt)
   for k,v in pairs(opt) do self[k] = v end
   assert(self.cocoDir and self.subCatsJson)
   self.dataType, self.annType = 'train2014', 'instances' 
   annFile = paths.concat(self.cocoDir, 'annotations', self.annType..'_'..self.dataType..'.json')
   self.cocoApi=coco.CocoApi(annFile)
   local subCats=json.decode(io.open(self.subCatsJson, 'r'):read())
   self.catId2classId = subCats.catId2classId
   self.annId2imgId = subCats.annId2imgId
   self.annNum = 0
   self.anns = {}
   for _ in pairs(self.annId2imgId) do 
       self.annNum=self.annNum+1
       table.insert(self.anns, _)
   end
end

function dataset:size()
    return self.annNum
end

function dataset:sample(i)
    collectgarbage()
    -- annObj must have segmentation and bbox !
    local annId, annObj
    if i then
        annId = self.anns[i]
        annObj = self.cocoApi:loadAnns(annId)[1]
    else
        annId = self.anns[torch.random(self.annNum)]
        annObj = self.cocoApi:loadAnns(annId)[1]
    end

    local imgId = self.annId2imgId[annId]
    local imgObj = self.cocoApi:loadImgs(imgId)[1]   
    local img = image.load(paths.concat(self.cocoDir, self.dataType, imgObj.file_name), 3, 'float') -- 3,h,w [0,1]
    local x1, y1, w, h = annObj.bbox[1], annObj.bbox[2], annObj.bbox[3], annObj.bbox[4]
    local x2, y2 = x1+w-1, y1+h-1
    local mask = coco.MaskApi.decode(coco.MaskApi.frPoly(annObj.segmentation, imgObj.height, imgObj.width)):squeeze() -- h,w 0/1 ByteTensor
    local label = self.catId2classId[annObj.category_id..'']
    img:mul(2):add(-1)  -- 3,h,w [-1, 1]
    x1, y1, x2, y2 = x1+1, y1+1, x2+1, y2+1  -- index start from 1

    -- resize img to fineSize
    h, w = img:size(2), img:size(3)
    h_factor, w_factor = opt.fineSize/h, opt.fineSize/w
    local x1F, x2F, y1F, y2F, imF, maskF
    x1F = math.ceil((x1-1)*w_factor+1)
    x2F = math.ceil((x2-1)*w_factor+1)
    y1F = math.ceil((y1-1)*h_factor+1)
    y2F = math.ceil((y2-1)*h_factor+1)
    x1F = math.min(x1F, opt.fineSize)
    x2F = math.min(x2F, opt.fineSize)
    y1F = math.min(y1F, opt.fineSize)
    y2F = math.min(y2F, opt.fineSize)
    imF = image.scale(img, opt.fineSize, opt.fineSize, 'bicubic')
    maskF = image.scale(mask, opt.fineSize, opt.fineSize, 'bicubic')

    return imF, maskF, label, x1F, y1F, x2F, y2F
end
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------
-- trainLoader
-------------------------------------------------------------------------------------------
trainLoader = dataLoader(opt)
collectgarbage()

