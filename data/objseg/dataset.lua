--[[
    Copyright (c) 2015-present, Facebook, Inc.
    All rights reserved.

    This source code is licensed under the BSD-style license found in the
    LICENSE file in the root directory of this source tree. An additional grant
    of patent rights can be found in the PATENTS file in the same directory.
]]--

require 'torch'
torch.setdefaulttensortype('torch.FloatTensor')
local ffi = require 'ffi'
local class = require('pl.class')
local dir = require 'pl.dir'
local tablex = require 'pl.tablex'
local argcheck = require 'argcheck'
require 'sys'
require 'xlua'
require 'pl'
local image = require 'image'

local dataset = torch.class('dataLoader')

local initcheck = argcheck{
   pack=true,
   {name="maskPath", type="string", help="mask path"},

   {name="maskObjPath", type="string", help="mask obj path"},

   {name="imagePath", type="string", help="background image path"},

   {name="sampleSize",
    type="table",
    help="a consistent sample size to resize the images"},

   {name="verbose",
    type="boolean",
    help="Verbose mode during initialization",
    default = false},

   {name="loadSize",
    type="table",
    help="a size to load the images to, initially",
    opt = true},

   {name="sampleHookImage",
    type="function",
    help="applied to sample during training(ex: for lighting jitter). "
       .. "It takes the image path as input",
    opt = true},

    {name="sampleHookMask", type="function", opt=true}
}

local function listImages(path)
   local extensionList = {'jpg', 'png','JPG','PNG','JPEG', 'ppm', 'PPM', 'bmp', 'BMP'}
   local findOptions = ' -iname "*.' .. extensionList[1] .. '"'
   for i=2,#extensionList do
      findOptions = findOptions .. ' -o -iname "*.' .. extensionList[i] .. '"'
   end
   local imagePaths = torch.CharTensor()  -- path to each image
   local imageList = os.tmpname();

   local command = 'find -H "' .. path .. '" ' .. findOptions .. ' >>"' .. imageList .. '" \n'
   os.execute(command)

   local maxPathLength = tonumber(sys.fexecute("wc -L '" .. imageList .. "' | cut -f1 -d' '")) + 1
   local length = tonumber(sys.fexecute("wc -l '" .. imageList .. "' | cut -f1 -d' '"))
   assert(length > 0, "Could not find any image file in the given input path")
   assert(maxPathLength > 0, "paths of files are length 0?")
   imagePaths:resize(length, maxPathLength):fill(0)
   local s_data = imagePaths:data()
   local count = 0
   for line in io.lines(imageList) do
      ffi.copy(s_data, line)
      s_data = s_data + maxPathLength
      count = count + 1
   end
   print(string.format('list %d images from %s', count, path))
   os.execute('rm -f "' .. imageList .. '"')
   return imagePaths 
end


function dataset:__init(...)
   local args =  initcheck(...)
   print(args)
   for k,v in pairs(args) do self[k] = v end
   if not self.loadSize then self.loadSize = self.sampleSize; end
   
   self.imagePaths = listImages(self.imagePath)
   self.maskPaths = listImages(self.maskPath)
   self.imageNum, self.maskNum = self.imagePaths:size(1), self.maskPaths:size(1)
end

function dataset:size()
    return self.imageNum
end

function dataset:sample(quantity)
   assert(quantity)
   local image = torch.Tensor(quantity, self.sampleSize[1], self.sampleSize[2], self.sampleSize[3]) -- (batch, channel, h, w)
   local maskobj = torch.Tensor(quantity, self.sampleSize[1], self.sampleSize[2], self.sampleSize[3]) -- (batch, channel, h, w)
   local mask = torch.ByteTensor(quantity, self.sampleSize[2], self.sampleSize[3]) -- (batch, h, w)
   for i=1,quantity do
      local imagepath = ffi.string(torch.data(self.imagePaths[torch.random(1, self.imageNum)]))
      local maskpath = ffi.string(torch.data(self.maskPaths[torch.random(1, self.maskNum)]))
      local maskobjpath = path.join(self.maskObjPath, path.basename(maskpath))
      image[i]:copy(self:sampleHookImage(imagepath))
      _, __ = self:sampleHookMask(maskpath, maskobjpath)
      mask[i]:copy(_)
      maskobj[i]:copy(__)
   end
   return image, mask, maskobj
end

return dataset
