require 'nngraph'
require 'ROI.ROIUpsampling'
require 'ROI.ROIReplace'
require 'ROI.ROIResizeReplace'
require 'detection'
require 'cudnn'

function defineG_unet_256(input_nc, output_nc, ngf)
    local netG = nil
    -- input is (nc) x 256 x 256
    local e1 = - nn.SpatialConvolution(input_nc, ngf, 4, 4, 2, 2, 1, 1)
    -- input is (ngf) x 128 x 128
    local e2 = e1 - nn.LeakyReLU(0.2, true) - nn.SpatialConvolution(ngf, ngf * 2, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 2)
    -- input is (ngf * 2) x 64 x 64
    local e3 = e2 - nn.LeakyReLU(0.2, true) - nn.SpatialConvolution(ngf * 2, ngf * 4, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 4)
    -- input is (ngf * 4) x 32 x 32
    local e4 = e3 - nn.LeakyReLU(0.2, true) - nn.SpatialConvolution(ngf * 4, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8)
    -- input is (ngf * 8) x 16 x 16
    local e5 = e4 - nn.LeakyReLU(0.2, true) - nn.SpatialConvolution(ngf * 8, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8)
    -- input is (ngf * 8) x 8 x 8
    local e6 = e5 - nn.LeakyReLU(0.2, true) - nn.SpatialConvolution(ngf * 8, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8)
    -- input is (ngf * 8) x 4 x 4
    local e7 = e6 - nn.LeakyReLU(0.2, true) - nn.SpatialConvolution(ngf * 8, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8)
    -- input is (ngf * 8) x 2 x 2
    local e8 = e7 - nn.LeakyReLU(0.2, true) - nn.SpatialConvolution(ngf * 8, ngf * 8, 4, 4, 2, 2, 1, 1) -- nn.SpatialBatchNormalization(ngf * 8)
    -- input is (ngf * 8) x 1 x 1
    
    local d1_ = e8 - nn.ReLU(true) - nn.SpatialFullConvolution(ngf * 8, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8) - nn.Dropout(0.5)
    -- input is (ngf * 8) x 2 x 2
    local d1 = {d1_,e7} - nn.JoinTable(2)
    local d2_ = d1 - nn.ReLU(true) - nn.SpatialFullConvolution(ngf * 8 * 2, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8) - nn.Dropout(0.5)
    -- input is (ngf * 8) x 4 x 4
    local d2 = {d2_,e6} - nn.JoinTable(2)
    local d3_ = d2 - nn.ReLU(true) - nn.SpatialFullConvolution(ngf * 8 * 2, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8) - nn.Dropout(0.5)
    -- input is (ngf * 8) x 8 x 8
    local d3 = {d3_,e5} - nn.JoinTable(2)
    local d4_ = d3 - nn.ReLU(true) - nn.SpatialFullConvolution(ngf * 8 * 2, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8)
    -- input is (ngf * 8) x 16 x 16
    local d4 = {d4_,e4} - nn.JoinTable(2)
    local d5_ = d4 - nn.ReLU(true) - nn.SpatialFullConvolution(ngf * 8 * 2, ngf * 4, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 4)
    -- input is (ngf * 4) x 32 x 32
    local d5 = {d5_,e3} - nn.JoinTable(2)
    local d6_ = d5 - nn.ReLU(true) - nn.SpatialFullConvolution(ngf * 4 * 2, ngf * 2, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 2)
    -- input is (ngf * 2) x 64 x 64
    local d6 = {d6_,e2} - nn.JoinTable(2)
    local d7_ = d6 - nn.ReLU(true) - nn.SpatialFullConvolution(ngf * 2 * 2, ngf, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf)
    -- input is (ngf) x128 x 128
    local d7 = {d7_,e1} - nn.JoinTable(2)
    local d8 = d7 - nn.ReLU(true) - nn.SpatialFullConvolution(ngf * 2, output_nc, 4, 4, 2, 2, 1, 1)
    -- input is (nc) x 256 x 256
    
    local o1 = d8 - nn.Tanh()
    
    netG = nn.gModule({e1},{o1})
    
    --graph.dot(netG.fg,'netG')
    
    return netG
end


function defineG_unet_256_FCfeat(input_nc, output_nc, ngf, feat_nc)
    local netG = nil
    -- input is (nc) x 256 x 256 and (feat_nc) x 256 x 256
    local f1 = - nn.Identity()  --for feature 
    local e1 = - nn.SpatialConvolution(input_nc, ngf, 4, 4, 2, 2, 1, 1) --for image
    -- input is (ngf) x 128 x 128
    local e2 = e1 - nn.LeakyReLU(0.2, true) - nn.SpatialConvolution(ngf, ngf * 2, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 2)
    -- input is (ngf * 2) x 64 x 64
    local e3 = e2 - nn.LeakyReLU(0.2, true) - nn.SpatialConvolution(ngf * 2, ngf * 4, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 4)
    -- input is (ngf * 4) x 32 x 32
    local e4 = e3 - nn.LeakyReLU(0.2, true) - nn.SpatialConvolution(ngf * 4, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8)
    -- input is (ngf * 8) x 16 x 16
    local e5 = e4 - nn.LeakyReLU(0.2, true) - nn.SpatialConvolution(ngf * 8, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8)
    -- input is (ngf * 8) x 8 x 8
    local e6 = e5 - nn.LeakyReLU(0.2, true) - nn.SpatialConvolution(ngf * 8, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8)
    -- input is (ngf * 8) x 4 x 4
    local e7 = e6 - nn.LeakyReLU(0.2, true) - nn.SpatialConvolution(ngf * 8, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8)
    -- input is (ngf * 8) x 2 x 2
    local e8 = e7 - nn.LeakyReLU(0.2, true) - nn.SpatialConvolution(ngf * 8, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8)
    -- input is (ngf * 8) x 1 x 1
    local e9 = {e8,f1} - nn.JoinTable(2)
    
    local d1_ = e9 - nn.ReLU(true) - nn.SpatialFullConvolution(ngf * 8+feat_nc, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8) - nn.Dropout(0.5)
    -- input is (ngf * 8) x 2 x 2
    local d1 = {d1_,e7} - nn.JoinTable(2)
    local d2_ = d1 - nn.ReLU(true) - nn.SpatialFullConvolution(ngf * 8 * 2, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8) - nn.Dropout(0.5)
    -- input is (ngf * 8) x 4 x 4
    local d2 = {d2_,e6} - nn.JoinTable(2)
    local d3_ = d2 - nn.ReLU(true) - nn.SpatialFullConvolution(ngf * 8 * 2, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8) - nn.Dropout(0.5)
    -- input is (ngf * 8) x 8 x 8
    local d3 = {d3_,e5} - nn.JoinTable(2)
    local d4_ = d3 - nn.ReLU(true) - nn.SpatialFullConvolution(ngf * 8 * 2, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8)
    -- input is (ngf * 8) x 16 x 16
    local d4 = {d4_,e4} - nn.JoinTable(2)
    local d5_ = d4 - nn.ReLU(true) - nn.SpatialFullConvolution(ngf * 8 * 2, ngf * 4, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 4)
    -- input is (ngf * 4) x 32 x 32
    local d5 = {d5_,e3} - nn.JoinTable(2)
    local d6_ = d5 - nn.ReLU(true) - nn.SpatialFullConvolution(ngf * 4 * 2, ngf * 2, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 2)
    -- input is (ngf * 2) x 64 x 64
    local d6 = {d6_,e2} - nn.JoinTable(2)
    local d7_ = d6 - nn.ReLU(true) - nn.SpatialFullConvolution(ngf * 2 * 2, ngf, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf)
    -- input is (ngf) x128 x 128
    local d7 = {d7_,e1} - nn.JoinTable(2)
    local d8 = d7 - nn.ReLU(true) - nn.SpatialFullConvolution(ngf * 2, output_nc, 4, 4, 2, 2, 1, 1)
    -- input is (nc) x 256 x 256
    
    local o1 = d8 - nn.Tanh()
    
    netG = nn.gModule({e1, f1},{o1})
    
    --graph.dot(netG.fg,'netG')
    
    return netG
end


function defineG_unet_256_FCfeat_ROI(input_nc, output_nc, ngf, feat_nc, debug)
    local netG = nil
    -- input is (nc) x 256 x 256 and (feat_nc) x 256 x 256
    local feat = - nn.Identity()  --for feature 
    local bbox = - nn.Identity()
    local e1 = - nn.SpatialConvolution(input_nc, ngf, 4, 4, 2, 2, 1, 1) --for image
    -- input is (ngf) x 128 x 128
    local e2 = e1 - nn.LeakyReLU(0.2, true) - nn.SpatialConvolution(ngf, ngf * 2, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 2)
    -- input is (ngf * 2) x 64 x 64
    local e3 = e2 - nn.LeakyReLU(0.2, true) - nn.SpatialConvolution(ngf * 2, ngf * 4, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 4)
    -- input is (ngf * 4) x 32 x 32
    local e4 = e3 - nn.LeakyReLU(0.2, true) - nn.SpatialConvolution(ngf * 4, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8)
    -- input is (ngf * 8) x 16 x 16
    local e5 = e4 - nn.LeakyReLU(0.2, true) - nn.SpatialConvolution(ngf * 8, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8)
    -- input is (ngf * 8) x 8 x 8
    local e6 = e5 - nn.LeakyReLU(0.2, true) - nn.SpatialConvolution(ngf * 8, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8)
    -- input is (ngf * 8) x 4 x 4
    local e7 = e6 - nn.LeakyReLU(0.2, true) - nn.SpatialConvolution(ngf * 8, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8)
    -- input is (ngf * 8) x 2 x 2
    local e8 = e7 - nn.LeakyReLU(0.2, true) - nn.SpatialConvolution(ngf * 8, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8)
    -- input is (ngf * 8) x 1 x 1
    local e9 = {e8,feat} - nn.JoinTable(2)
    --------------------------------------------------------------------
    local d1 = e9 - nn.LeakyReLU(0.2, true) - nn.SpatialFullConvolution(ngf * 8+feat_nc, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8) - nn.Dropout(0.5)
    -- input is (ngf * 8) x 2 x 2
    local d2 = d1 - nn.LeakyReLU(0.2, true) - nn.SpatialFullConvolution(ngf * 8, ngf * 8, 3, 3, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8) - nn.Dropout(0.5)
    -- input is (ngf * 8) x 3 x 3
    local d3 = d2 - nn.LeakyReLU(0.2, true) - nn.SpatialFullConvolution(ngf * 8, ngf * 8, 3, 3, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8) - nn.Dropout(0.5)
    -- input is (ngf * 8) x 5 x 5
    local d4_ = {d3, bbox} - ROIUpsampling(5, 5, 16, 16, 1/16) 
    -- input is (ngf * 8) x 16 x 16
    --------------------------------------------------------------------
    local d4 = {d4_,e4} - nn.JoinTable(2)
    local d5_ = d4 - nn.LeakyReLU(0.2, true) - nn.SpatialFullConvolution(ngf * 8 * 2, ngf * 4, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 4)
    -- input is (ngf * 4) x 32 x 32
    local d5 = {d5_,e3} - nn.JoinTable(2)
    local d6_ = d5 - nn.LeakyReLU(0.2, true) - nn.SpatialFullConvolution(ngf * 4 * 2, ngf * 2, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 2)
    -- input is (ngf * 2) x 64 x 64
    local d6 = {d6_,e2} - nn.JoinTable(2)
    local d7_ = d6 - nn.LeakyReLU(0.2, true) - nn.SpatialFullConvolution(ngf * 2 * 2, ngf, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf)
    -- input is (ngf) x128 x 128
    local d7 = {d7_,e1} - nn.JoinTable(2)
    local d8 = d7 - nn.LeakyReLU(0.2, true) - nn.SpatialFullConvolution(ngf * 2, output_nc, 4, 4, 2, 2, 1, 1)
    -- input is (nc) x 256 x 256
    local o1 = d8 - nn.Tanh()
    if debug then
        netG = nn.gModule({e1, feat, bbox}, {o1, d3, d4_})
    else
        netG = nn.gModule({e1, feat, bbox},{o1})
    end
    return netG
end

function defineG_unet_256_FCfeat_ROI_v2(input_nc, output_nc, ngf, feat_nc, debug)
    local netG = nil
    -- input is (nc) x 256 x 256 and (feat_nc) x 256 x 256
    local feat = - nn.Identity()  --for feature 
    local bbox = - nn.Identity()
    local e1 = - nn.SpatialConvolution(input_nc, ngf, 4, 4, 2, 2, 1, 1) --for image
    -- input is (ngf) x 128 x 128
    local e2 = e1 - nn.LeakyReLU(0.2, true) - nn.SpatialConvolution(ngf, ngf * 2, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 2)
    -- input is (ngf * 2) x 64 x 64
    local e3 = e2 - nn.LeakyReLU(0.2, true) - nn.SpatialConvolution(ngf * 2, ngf * 4, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 4)
    -- input is (ngf * 4) x 32 x 32
    local e4 = e3 - nn.LeakyReLU(0.2, true) - nn.SpatialConvolution(ngf * 4, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8)
    -- input is (ngf * 8) x 16 x 16
    --------------------------------------------------------------------
    local d1 = feat - nn.LeakyReLU(0.2, true) - nn.SpatialFullConvolution(feat_nc, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8) - nn.Dropout(0.5)
    -- input is (ngf * 8) x 2 x 2
    local d2 = d1 - nn.LeakyReLU(0.2, true) - nn.SpatialFullConvolution(ngf * 8, ngf * 8, 3, 3, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8) - nn.Dropout(0.5)
    -- input is (ngf * 8) x 3 x 3
    local d3 = d2 - nn.LeakyReLU(0.2, true) - nn.SpatialFullConvolution(ngf * 8, ngf * 8, 3, 3, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8) - nn.Dropout(0.5)
    -- input is (ngf * 8) x 5 x 5
    local d4_ = {d3, bbox} - ROIUpsampling(5, 5, 16, 16, 1/16) 
    -- input is (ngf * 8) x 16 x 16
    --------------------------------------------------------------------
    local d4 = {d4_,e4} - nn.JoinTable(2)
    local d5_ = d4 - nn.LeakyReLU(0.2, true) - nn.SpatialFullConvolution(ngf * 8 * 2, ngf * 4, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 4)
    -- input is (ngf * 4) x 32 x 32
    local d5 = {d5_,e3} - nn.JoinTable(2)
    local d6_ = d5 - nn.LeakyReLU(0.2, true) - nn.SpatialFullConvolution(ngf * 4 * 2, ngf * 2, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 2)
    -- input is (ngf * 2) x 64 x 64
    local d6 = {d6_,e2} - nn.JoinTable(2)
    local d7_ = d6 - nn.LeakyReLU(0.2, true) - nn.SpatialFullConvolution(ngf * 2 * 2, ngf, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf)
    -- input is (ngf) x128 x 128
    local d7 = {d7_,e1} - nn.JoinTable(2)
    local d8 = d7 - nn.LeakyReLU(0.2, true) - nn.SpatialFullConvolution(ngf * 2, output_nc, 4, 4, 2, 2, 1, 1)
    -- input is (nc) x 256 x 256
    local o1 = d8 - nn.Tanh()
    
    if debug then
        netG = nn.gModule({e1, feat, bbox}, {o1, e4, d4_})
    else
        netG = nn.gModule({e1, feat, bbox},{o1})
    end
    return netG
end

function residual_block(input, inc, onc, tp)
    local output
    if tp == 'conv' then
        local i_1 = input - nn.SpatialConvolution(inc, inc, 3,3,1,1,1,1) - nn.SpatialBatchNormalization(inc) - nn.LeakyReLU(0.2,true)
        local i_2 = i_1 - nn.SpatialConvolution(inc, inc, 3,3,1,1,1,1) - nn.SpatialBatchNormalization(inc) - nn.LeakyReLU(0.2,true)
        output = {input, i_2} - nn.CAddTable()-nn.SpatialConvolution(inc, onc, 4,4,2,2,1,1) - nn.SpatialBatchNormalization(onc)
    elseif tp == 'deconv' then
        local i_1 = input - nn.SpatialFullConvolution(inc, inc, 3,3,1,1,1,1) - nn.SpatialBatchNormalization(inc) - nn.ReLU(true)
        local i_2 = i_1 - nn.SpatialFullConvolution(inc, inc, 3,3,1,1,1,1) - nn.SpatialBatchNormalization(inc) - nn.ReLU(true)
        output = {input, i_2} - nn.CAddTable()-nn.SpatialFullConvolution(inc, onc, 4,4,2,2,1,1) - nn.SpatialBatchNormalization(onc) - nn.Dropout(0.5)
    elseif tp == 'deconv-out' then
        local i_1 = input - nn.SpatialFullConvolution(inc, inc, 3,3,1,1,1,1) - nn.SpatialBatchNormalization(inc) - nn.ReLU(true)
        local i_2 = i_1 - nn.SpatialFullConvolution(inc, inc, 3,3,1,1,1,1) - nn.SpatialBatchNormalization(inc) - nn.ReLU(true)
        output = {input, i_2} - nn.CAddTable()-nn.SpatialFullConvolution(inc, onc, 4,4,2,2,1,1) - nn.Tanh()
    end
    return output
end


function defineG_unet_256_FCfeat_ROI_replace(input_nc, output_nc, ngf, debug)
    local netG = nil
    -- input is (nc) x 256 x 256
    local feat = - nn.Identity()  --for feature 
    local bbox = - nn.Identity()
    local e1 = - nn.SpatialConvolution(input_nc, ngf, 4, 4, 2, 2, 1, 1) --for image
    -- input is (ngf) x 128 x 128
    local e2 = residual_block(e1 - nn.LeakyReLU(0.2, true), ngf, ngf * 2, 'conv')
    -- input is (ngf * 2) x 64 x 64
    local e3 = residual_block(e2 - nn.LeakyReLU(0.2, true), ngf * 2, ngf * 4, 'conv')
    -- input is (ngf * 4) x 32 x 32
    local e4 = residual_block(e3 - nn.LeakyReLU(0.2, true), ngf * 4, ngf * 8, 'conv')
    -- input is (ngf * 8) x 16 x 16
    --------------------------------------------------------------------
    -- feat must be 512 x 7 x 7, roi feat of object B
    local d3 = {feat, bbox} - ROIUpsampling(7, 7, 16, 16, 1/16)
    -- d3 is 512 x 16 x 16 and has the target feature at bbox A 
    -- now replace bbox A of e4
    local d4_ = {e4, d3, bbox} - ROIReplace(16, 16, 1/16)
    -- input is (ngf * 8) x 16 x 16
    --------------------------------------------------------------------
    local d5_ = residual_block(d4_ - nn.ReLU(true), ngf * 8, ngf * 4, 'deconv')
    -- input is (ngf * 4) x 32 x 32
    local d5 = {d5_,e3} - nn.JoinTable(2)
    local d6_ = residual_block(d5 - nn.ReLU(true), ngf * 4 * 2, ngf * 2, 'deconv')
    -- input is (ngf * 2) x 64 x 64
    local d6 = {d6_,e2} - nn.JoinTable(2)
    local d7_ = residual_block(d6 - nn.ReLU(true), ngf * 2 * 2, ngf, 'deconv')
    -- input is (ngf) x128 x 128
    local d7 = {d7_,e1} - nn.JoinTable(2)
    local d8 = d7 - nn.ReLU(true) - nn.SpatialFullConvolution(ngf * 2, output_nc, 4, 4, 2, 2, 1, 1)
    -- input is (nc) x 256 x 256
    local o1 = d8 - nn.Tanh()
    
    if debug then
        netG = nn.gModule({e1, feat, bbox}, {o1, e4, d4_, d3})
    else
        netG = nn.gModule({e1, feat, bbox},{o1})
    end
    return netG
end


function Decoder_ROI_replace_difference(output_nc, ngf, debug)
    local netG = nil
    -- input is (nc) x 256 x 256
    local img = - nn.Identity()
    local featA = - nn.Identity()  --for feature 
    local bboxA = - nn.Identity()
    local featB = - nn.Identity()
    local bboxB = - nn.Identity()
    -- feat is (ngf * 8) x 16 x 16
    --------------------------------------------------------------------
    -- now replace bbox A of e4
    local d4_ = {featA, bboxA, featB, bboxB} - ROIResizeReplace(16, 16, 1/16)
    -- input is (ngf * 8) x 16 x 16
    --------------------------------------------------------------------
    local d4 = {d4_, featA} - nn.JoinTable(2)  -- NOTE: preserve featA, especially when B is different from A
    local d5 = residual_block(d4 - nn.ReLU(true), ngf * 8 * 2, ngf * 4, 'deconv')
    -- input is (ngf * 4) x 32 x 32
    local d6 = residual_block(d5 - nn.ReLU(true), ngf * 4, ngf * 2, 'deconv')
    -- input is (ngf * 2) x 64 x 64
    local d7 = residual_block(d6 - nn.ReLU(true), ngf * 2, ngf, 'deconv')
    -- input is (ngf) x128 x 128
    local d8 = d7 - nn.ReLU(true) - nn.SpatialFullConvolution(ngf, output_nc, 4, 4, 2, 2, 1, 1)
    -- input is (nc) x 256 x 256
    local o1 = d8 - nn.Tanh()
    local o2 = {img, o1} - nn.CAddTable() -- learn a difference
    
    if debug then
        netG = nn.gModule({img, featA, bboxA, featB, bboxB},{o2, o1})
    else
        netG = nn.gModule({img, featA, bboxA, featB, bboxB},{o2})
    end
    return netG
end

function Decoder_ROI_replace_Ufusion(output_nc, ngf, debug)
    local netG = nil
    -- input is (nc) x 256 x 256
    local img = - nn.Identity()
    local e1 = residual_block(img, 3, ngf, 'conv')  -- img feature
    local featA = - nn.Identity()  --for feature 
    local bboxA = - nn.Identity()
    local featB = - nn.Identity()
    local bboxB = - nn.Identity()
    -- feat is (ngf * 8) x 16 x 16
    --------------------------------------------------------------------
    -- now replace bbox A of e4
    local d4_ = {featA, bboxA, featB, bboxB} - ROIResizeReplace(16, 16, 1/16)
    -- input is (ngf * 8) x 16 x 16
    --------------------------------------------------------------------
    local d4 = {d4_, featA} - nn.JoinTable(2)  -- NOTE: preserve featA, especially when B is different from A
    local d5 = residual_block(d4 - nn.ReLU(true), ngf * 8 * 2, ngf * 4, 'deconv')
    -- input is (ngf * 4) x 32 x 32
    local d6 = residual_block(d5 - nn.ReLU(true), ngf * 4, ngf * 2, 'deconv')
    -- input is (ngf * 2) x 64 x 64
    local d7_ = residual_block(d6 - nn.ReLU(true), ngf * 2, ngf, 'deconv')
    -- input is (ngf) x128 x 128
    local d7 = {d7_, e1} - nn.JoinTable(2)  -- concat with img feature
    local o1 = residual_block(d7 - nn.ReLU(true), ngf * 2, output_nc, 'deconv-out')
    -- input is (nc) x 256 x 256
    if debug then
        netG = nn.gModule({img, featA, bboxA, featB, bboxB},{o1})
    else
        netG = nn.gModule({img, featA, bboxA, featB, bboxB},{o1})
    end
    return netG
end

function Decoder_ROI_replace_deeper_Ufusion(output_nc, ngf, debug)
    local netG = nil
    -- input is (nc) x 256 x 256
    local img = - nn.Identity()
    local e1 = residual_block(img, 3, ngf, 'conv')  -- img feature
    -- input is (ngf) x 128 x 128
    local e2 = residual_block(e1, ngf, ngf * 2, 'conv')
    -- input is (ngf * 2) x 64 x 64
    local featA = - nn.Identity()  --for feature 
    local bboxA = - nn.Identity()
    local featB = - nn.Identity()
    local bboxB = - nn.Identity()
    -- feat is (ngf * 8) x 16 x 16
    --------------------------------------------------------------------
    -- now replace bbox A of e4
    local d4_ = {featA, bboxA, featB, bboxB} - ROIResizeReplace(16, 16, 1/16)
    -- input is (ngf * 8) x 16 x 16
    --------------------------------------------------------------------
    local d4 = {d4_, featA} - nn.JoinTable(2)  -- NOTE: preserve featA, especially when B is different from A
    local d5 = residual_block(d4 - nn.ReLU(true), ngf * 8 * 2, ngf * 4, 'deconv')
    -- input is (ngf * 4) x 32 x 32
    local d6_ = residual_block(d5 - nn.ReLU(true), ngf * 4, ngf * 2, 'deconv')
    -- input is (ngf * 2) x 64 x 64
    local d6 = {d6_, e2} - nn.JoinTable(2)
    local d7_ = residual_block(d6 - nn.ReLU(true), ngf * 2 * 2, ngf, 'deconv')
    -- input is (ngf) x128 x 128
    local d7 = {d7_, e1} - nn.JoinTable(2)  -- concat with img feature
    local o1 = residual_block(d7 - nn.ReLU(true), ngf * 2, output_nc, 'deconv-out')
    -- input is (nc) x 256 x 256
    if debug then
        netG = nn.gModule({img, featA, bboxA, featB, bboxB},{o1})
    else
        netG = nn.gModule({img, featA, bboxA, featB, bboxB},{o1})
    end
    return netG
end

function Decoder_ROI_replace_deeper_Ufusion_noA(output_nc, ngf, debug)
    local netG = nil
    -- input is (nc) x 256 x 256
    local img = - nn.Identity()
    local e1 = residual_block(img, 3, ngf, 'conv')  -- img feature
    -- input is (ngf) x 128 x 128
    local e2 = residual_block(e1, ngf, ngf * 2, 'conv')
    -- input is (ngf * 2) x 64 x 64
    local featA = - nn.Identity()  --for feature 
    local bboxA = - nn.Identity()
    local featB = - nn.Identity()
    local bboxB = - nn.Identity()
    -- feat is (ngf * 8) x 16 x 16
    --------------------------------------------------------------------
    -- now replace bbox A of e4
    local d4 = {featA, bboxA, featB, bboxB} - ROIResizeReplace(16, 16, 1/16)
    -- input is (ngf * 8) x 16 x 16
    --------------------------------------------------------------------
    local d5 = residual_block(d4 - nn.ReLU(true), ngf * 8, ngf * 4, 'deconv')
    -- input is (ngf * 4) x 32 x 32
    local d6_ = residual_block(d5 - nn.ReLU(true), ngf * 4, ngf * 2, 'deconv')
    -- input is (ngf * 2) x 64 x 64
    local d6 = {d6_, e2} - nn.JoinTable(2)
    local d7_ = residual_block(d6 - nn.ReLU(true), ngf * 2 * 2, ngf, 'deconv')
    -- input is (ngf) x128 x 128
    local d7 = {d7_, e1} - nn.JoinTable(2)  -- concat with img feature
    local o1 = residual_block(d7 - nn.ReLU(true), ngf * 2, output_nc, 'deconv-out')
    -- input is (nc) x 256 x 256
    if debug then
        netG = nn.gModule({img, featA, bboxA, featB, bboxB},{o1})
    else
        netG = nn.gModule({img, featA, bboxA, featB, bboxB},{o1})
    end
    return netG
end

function Decoder_ROI_concat(output_nc, ngf, debug)
    local netG = nil
    -- input is (nc) x 256 x 256
    local img = - nn.Identity()
    local featA = - nn.Identity()  --for feature 
    local featB = - nn.Identity()  -- 0 outside bboxB
    -- feat is (ngf * 8) x 16 x 16
    --------------------------------------------------------------------
    local d4 = {featB, featA} - nn.JoinTable(2)  -- NOTE: preserve featA, especially when B is different from A
    local d5 = residual_block(d4 - nn.ReLU(true), ngf * 8 * 2, ngf * 4, 'deconv')
    -- input is (ngf * 4) x 32 x 32
    local d6 = residual_block(d5 - nn.ReLU(true), ngf * 4, ngf * 2, 'deconv')
    -- input is (ngf * 2) x 64 x 64
    local d7 = residual_block(d6 - nn.ReLU(true), ngf * 2, ngf, 'deconv')
    -- input is (ngf) x128 x 128
    local d8 = d7 - nn.ReLU(true) - nn.SpatialFullConvolution(ngf, output_nc, 4, 4, 2, 2, 1, 1)
    -- input is (nc) x 256 x 256
    local o1 = d8 - nn.Tanh()
    local o2 = {img, o1} - nn.CAddTable()
    
    if debug then
        netG = nn.gModule({img, featA, featB},{o2, o1})
    else
        netG = nn.gModule({img, featA, featB},{o2})
    end
    return netG
end


function defineG_ROI_ACGAN(input_nc, output_nc, ngf, feat_nc, debug)
    local netG = nil
    -- input is (nc) x 256 x 256
    local feat = - nn.Identity()  --for feature
    local bbox = - nn.Identity()
    --------------------------------------------------------------------
    local d0 = feat - nn.View(-1, feat_nc) - nn.Linear(feat_nc, 4096) - nn.Dropout(0.5) - nn.View(-1, 4096, 1, 1)
    local d1 = d0 - nn.ReLU(true) - nn.SpatialFullConvolution(4096, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8)
    -- input is (ngf * 8) x 2 x 2
    local d2 = d1 - nn.ReLU(true) - nn.SpatialFullConvolution(ngf * 8, ngf * 8, 3, 3, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8)
    -- input is (ngf * 8) x 3 x 3
    local d3 = d2 - nn.ReLU(true) - nn.SpatialFullConvolution(ngf * 8, ngf * 8, 3, 3, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8)
    -- input is (ngf * 8) x 5 x 5
    local d4 = {d3, bbox} - ROIUpsampling(5, 5, 16, 16, 1/16) 
    -- input is (ngf * 8) x 16 x 16
    --------------------------------------------------------------------
    local d5 = d4 - nn.ReLU(true) - nn.SpatialFullConvolution(ngf * 8, ngf * 4, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 4)
    -- input is (ngf * 4) x 32 x 32
    local d6 = d5 - nn.ReLU(true) - nn.SpatialFullConvolution(ngf * 4, ngf * 2, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 2)
    -- input is (ngf * 2) x 64 x 64
    local d7 = d6 - nn.ReLU(true) - nn.SpatialFullConvolution(ngf * 2, ngf, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf)
    -- input is (ngf) x128 x 128
    local d8 = d7 - nn.ReLU(true) - nn.SpatialFullConvolution(ngf, output_nc, 4, 4, 2, 2, 1, 1)
    -- input is (nc) x 256 x 256
    local o1 = d8 - nn.Tanh()
    if debug then
        netG = nn.gModule({feat, bbox}, {o1, d1, d4})
    else
        netG = nn.gModule({feat, bbox},{o1})
    end
    return netG
end

function defineG_unet_256_FCfeat_residualblock(input_nc, output_nc, ngf, feat_nc)
    local netG = nil
    -- input is (nc) x 256 x 256 and (feat_nc) x 256 x 256
    local f1 = - nn.Identity()  --for feature 
    local e1 = - nn.SpatialConvolution(input_nc, ngf, 4, 4, 2, 2, 1, 1) --for image
    -- input is (ngf) x 128 x 128
    local e2 = residual_block(e1 - nn.LeakyReLU(0.2, true), ngf, ngf * 2, 'conv')
    -- input is (ngf * 2) x 64 x 64
    local e3 = residual_block(e2 - nn.LeakyReLU(0.2, true), ngf * 2, ngf * 4, 'conv')
    -- input is (ngf * 4) x 32 x 32
    local e4 = residual_block(e3 - nn.LeakyReLU(0.2, true), ngf * 4, ngf * 8, 'conv')

    --[[ input is (ngf * 8) x 16 x 16
    local e5 = residual_block(e4 - nn.LeakyReLU(0.2, true), ngf * 8, ngf * 8, 'conv')
    -- input is (ngf * 8) x 8 x 8
    local e6 = residual_block(e5 - nn.LeakyReLU(0.2, true), ngf * 8, ngf * 8, 'conv')
    -- input is (ngf * 8) x 4 x 4
    local e7 = residual_block(e6 - nn.LeakyReLU(0.2, true), ngf * 8, ngf * 8, 'conv')
    -- input is (ngf * 8) x 2 x 2
    local e8 = residual_block(e7 - nn.LeakyReLU(0.2, true), ngf * 8, ngf * 8, 'conv')
    ]]
    local e5 = e4 - nn.LeakyReLU(0.2, true) - nn.SpatialConvolution(ngf * 8, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8)
    -- input is (ngf * 8) x 8 x 8
    local e6 = e5 - nn.LeakyReLU(0.2, true) - nn.SpatialConvolution(ngf * 8, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8)
    -- input is (ngf * 8) x 4 x 4
    local e7 = e6 - nn.LeakyReLU(0.2, true) - nn.SpatialConvolution(ngf * 8, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8)
    -- input is (ngf * 8) x 2 x 2
    local e8 = e7 - nn.LeakyReLU(0.2, true) - nn.SpatialConvolution(ngf * 8, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8)


    -- input is (ngf * 8) x 1 x 1
    local e9 = {e8,f1} - nn.JoinTable(2)
    -- ngf*8 + feat_nc
    
    --[[local d1_ = residual_block(e9 - nn.ReLU(true), ngf * 8 + feat_nc, ngf * 8, 'deconv')
    -- input is (ngf * 8) x 2 x 2
    local d1 = {d1_,e7} - nn.JoinTable(2)
    local d2_ = residual_block(d1 - nn.ReLU(true), ngf * 8 * 2, ngf * 8, 'deconv')
    -- input is (ngf * 8) x 4 x 4
    local d2 = {d2_,e6} - nn.JoinTable(2)
    local d3_ = residual_block(d2 - nn.ReLU(true), ngf * 8 * 2, ngf * 8, 'deconv')
    -- input is (ngf * 8) x 8 x 8
    local d3 = {d3_,e5} - nn.JoinTable(2)
    local d4_ = residual_block(d3 - nn.ReLU(true), ngf * 8 * 2, ngf * 8, 'deconv')
    ]]
    local d1_ = e9 - nn.ReLU(true) - nn.SpatialFullConvolution(ngf * 8+feat_nc, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8) - nn.Dropout(0.5)
    -- input is (ngf * 8) x 2 x 2
    local d1 = {d1_,e7} - nn.JoinTable(2)
    local d2_ = d1 - nn.ReLU(true) - nn.SpatialFullConvolution(ngf * 8 * 2, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8) - nn.Dropout(0.5)
    -- input is (ngf * 8) x 4 x 4
    local d2 = {d2_,e6} - nn.JoinTable(2)
    local d3_ = d2 - nn.ReLU(true) - nn.SpatialFullConvolution(ngf * 8 * 2, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8) - nn.Dropout(0.5)
    -- input is (ngf * 8) x 8 x 8
    local d3 = {d3_,e5} - nn.JoinTable(2)
    local d4_ = d3 - nn.ReLU(true) - nn.SpatialFullConvolution(ngf * 8 * 2, ngf * 8, 4, 4, 2, 2, 1, 1) - nn.SpatialBatchNormalization(ngf * 8)


    -- input is (ngf * 8) x 16 x 16
    local d4 = {d4_,e4} - nn.JoinTable(2)
    local d5_ = residual_block(d4 - nn.ReLU(true), ngf * 8 * 2, ngf * 4, 'deconv')
    -- input is (ngf * 4) x 32 x 32
    local d5 = {d5_,e3} - nn.JoinTable(2)
    local d6_ = residual_block(d5 - nn.ReLU(true), ngf * 4 * 2, ngf * 2, 'deconv')
    -- input is (ngf * 2) x 64 x 64
    local d6 = {d6_,e2} - nn.JoinTable(2)
    local d7_ = residual_block(d6 - nn.ReLU(true), ngf * 2 * 2, ngf, 'deconv')
    -- input is (ngf) x128 x 128
    local d7 = {d7_,e1} - nn.JoinTable(2)
    local d8 = d7 - nn.ReLU(true) - nn.SpatialFullConvolution(ngf * 2, output_nc, 4, 4, 2, 2, 1, 1)
    -- input is (nc) x 256 x 256
    
    local o1 = d8 - nn.Tanh()
    
    netG = nn.gModule({e1, f1},{o1, d3})
    
    --graph.dot(netG.fg,'netG')
    
    return netG
end


function defineD_256(input_nc, ndf)
    local netD = nn.Sequential()
    -- input is (nc) x 256 x 256
    netD:add(cudnn.SpatialConvolution(input_nc, ndf, 4, 4, 2, 2, 1, 1))
    netD:add(nn.LeakyReLU(0.2, true))
    -- input is (ndf) x 128 x 128
    netD:add(cudnn.SpatialConvolution(ndf, ndf, 4, 4, 2, 2, 1, 1))
    netD:add(cudnn.SpatialBatchNormalization(ndf)):add(nn.LeakyReLU(0.2, true))
    -- state size: (ndf) x 64 x 64
    netD:add(cudnn.SpatialConvolution(ndf, ndf * 2, 4, 4, 2, 2, 1, 1))
    netD:add(cudnn.SpatialBatchNormalization(ndf*2)):add(nn.LeakyReLU(0.2, true))
    -- state size: (ndf*2) x 32 x 32
    netD:add(cudnn.SpatialConvolution(ndf*2, ndf * 4, 4, 4, 2, 2, 1, 1))
    netD:add(cudnn.SpatialBatchNormalization(ndf * 4)):add(nn.LeakyReLU(0.2, true))
    -- state size: (ndf*4) x 16 x 16
    netD:add(cudnn.SpatialConvolution(ndf * 4, ndf * 8, 4, 4, 2, 2, 1, 1))
    netD:add(cudnn.SpatialBatchNormalization(ndf * 8)):add(nn.LeakyReLU(0.2, true))
    -- state size: (ndf*8) x 8 x 8
    netD:add(cudnn.SpatialConvolution(ndf * 8, ndf * 8, 4, 4, 2, 2, 1, 1))
    netD:add(cudnn.SpatialBatchNormalization(ndf * 8)):add(nn.LeakyReLU(0.2, true))
    -- state size: (ndf*8) x 4 x 4
    netD:add(cudnn.SpatialConvolution(ndf * 8, 1, 4, 4))
    netD:add(nn.Sigmoid())
    -- state size: 1 x 1 x 1
    netD:add(nn.View(1):setNumInputDims(3))
    -- state size: 1
    
    return netD
end


function defineD_256_ROI(input_nc, ndf)
    ksize = 4

    local conv = nn.Sequential()
    -- input is (nc) x 256 x 256
    conv:add(nn.SpatialConvolution(input_nc, ndf, ksize, ksize, 2, 2, 1, 1))
    conv:add(nn.LeakyReLU(0.2, true))
    -- input is (ndf) x 128 x 128
    conv:add(nn.SpatialConvolution(ndf, ndf*2, ksize, ksize, 2, 2, 1, 1))
    conv:add(nn.SpatialBatchNormalization(ndf*2)):add(nn.LeakyReLU(0.2, true))
    -- state size: (ndf*2) x 64 x 64
    conv:add(nn.SpatialConvolution(ndf*2, ndf * 4, ksize, ksize, 2, 2, 1, 1))
    conv:add(nn.SpatialBatchNormalization(ndf*4)):add(nn.LeakyReLU(0.2, true))
    -- state size: (ndf*4) x 32 x 32
    conv:add(nn.SpatialConvolution(ndf*4, ndf * 8, ksize, ksize, 2, 2, 1, 1))
    conv:add(nn.SpatialBatchNormalization(ndf * 8)):add(nn.LeakyReLU(0.2, true))
    -- state size: (ndf*8) x 16 x 16
    
    local roi_info = nn.ParallelTable()
    roi_info:add(conv)
    roi_info:add(nn.Identity())

    local ROIPooling = detection.ROIPooling(7,7):setSpatialScale(1/16)
    -- state size: (ndf*8) x 7 x 7

    local classifier = nn.Sequential()
    classifier:add(nn.SpatialConvolution(ndf * 8, ndf * 8, ksize, ksize, 2, 2, 1, 1))
    classifier:add(nn.SpatialBatchNormalization(ndf * 8)):add(nn.LeakyReLU(0.2, true))
    -- state size: (ndf*8) x 3 x 3
    classifier:add(nn.SpatialConvolution(ndf * 8, ndf * 8, 3, 3))
    classifier:add(nn.LeakyReLU(0.2, true))
    -- state size: (ndf*8) x 1 x 1
    classifier:add(nn.SpatialConvolution(ndf * 8, 1, 1, 1))
    classifier:add(nn.Sigmoid())
    -- state size: 1 x 1 x 1
    classifier:add(nn.View(1))
    -- state size: 1
    
    local netD = nn.Sequential()
    netD:add(roi_info)
    netD:add(ROIPooling)
    netD:add(classifier)

    return netD
end

function defineD_ROI_ACGAN(input_nc, ndf, nclass)
    local conv = nn.Sequential()
    -- input is (nc) x 256 x 256
    conv:add(nn.SpatialConvolution(input_nc, ndf, 4, 4, 2, 2, 1, 1))
    conv:add(nn.LeakyReLU(0.2, true))
    -- input is (ndf) x 128 x 128
    conv:add(nn.SpatialConvolution(ndf, ndf*2, 4, 4, 2, 2, 1, 1))
    conv:add(nn.SpatialBatchNormalization(ndf*2)):add(nn.LeakyReLU(0.2, true))
    -- state size: (ndf*2) x 64 x 64
    conv:add(nn.SpatialConvolution(ndf*2, ndf * 4, 4, 4, 2, 2, 1, 1))
    conv:add(nn.SpatialBatchNormalization(ndf*4)):add(nn.LeakyReLU(0.2, true))
    -- state size: (ndf*4) x 32 x 32
    conv:add(nn.SpatialConvolution(ndf*4, ndf * 8, 4, 4, 2, 2, 1, 1))
    conv:add(nn.SpatialBatchNormalization(ndf * 8)):add(nn.LeakyReLU(0.2, true))
    -- state size: (ndf*8) x 16 x 16
    
    local roi_info = nn.ParallelTable()
    roi_info:add(conv)
    roi_info:add(nn.Identity())

    local ROIPooling = detection.ROIPooling(7,7):setSpatialScale(1/16)
    -- state size: (ndf*8) x 7 x 7

    local linear = nn.Sequential()
    linear:add(nn.SpatialConvolution(ndf * 8, ndf * 8, 4, 4, 2, 2, 1, 1))
    linear:add(nn.SpatialBatchNormalization(ndf * 8)):add(nn.LeakyReLU(0.2, true))
    -- state size: (ndf*8) x 3 x 3
    linear:add(nn.SpatialConvolution(ndf * 8, ndf * 8, 3, 3))
    linear:add(nn.LeakyReLU(0.2, true))
    linear:add(nn.View(ndf * 8))
    -- state size: (ndf*8)

    -- [1]: classification logit       [2]: real/fake score 
    local output = nn.ConcatTable()
    output:add(nn.Linear(ndf * 8, nclass))
    local score = nn.Sequential()
    score:add(nn.Linear(ndf * 8, 1)):add(nn.Sigmoid())
    output:add(score)

    local netD = nn.Sequential()
    netD:add(roi_info)
    netD:add(ROIPooling)
    netD:add(linear)
    netD:add(output)

    return netD
end
