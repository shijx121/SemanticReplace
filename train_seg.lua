require 'torch'
require 'nn'
require 'optim'
require 'image'
require 'models'
util = paths.dofile('util.lua')

opt = {
    imagePath = '../image/256/background',
    maskPath = '../image/256/segbox',
   batchSize = 32,         -- number of samples to produce
   loadSize = 350,         -- resize the loaded image to loadsize maintaining aspect ratio. 0 means don't resize. -1 means scale randomly between [0.5,2] -- see donkey_folder.lua
   fineSize = 256,         -- size of random crops. Only 128 or 256 supported.
   ngf = 64,               -- #  of gen filters in first conv layer
   ndf = 64,               -- #  of discrim filters in first conv layer
   nc = 3,                 -- # of channels in input
   wRecon = 0.999,               -- 0 means don't use else use with this weight
   useOverlapPred = 0,        -- overlapping edges (1 means yes, 0 means no). 1 means put 10x more recon weight on unmasked region.
   nThreads = 4,           -- #  of data loading threads to use
   niter = 120,             -- #  of iter at starting learning rate
   lr = 0.00002,            -- initial learning rate for adam
   lrd = 5e-4,             -- learning rate decay, make lr multiply 1/11 after 2e5 updates
   beta1 = 0.5,            -- momentum term of adam
   ntrain = math.huge,     -- #  of examples per epoch. math.huge for full dataset
   display = 1,            -- display samples while training. 0 = false
   display_id = 20,        -- display window id.
   display_iter = 50,      -- # number of iterations after which display is updated
   gpu = 2,                -- gpu = 0 is CPU mode. gpu=X is GPU mode on GPU X
   name = 'Segx256_no-overlap_no-aug-Glr',        -- name of the experiment you are running
   manualSeed = 0,         -- 0 means random seed

   continue_GPath = './checkpoints/Segx256_no-overlap_no-aug-Glr/180_net_G.t7',
   continue_DPath = './checkpoints/Segx256_no-overlap_no-aug-Glr/180_net_D.t7',
   continue_start_iter = 180,
}
for k,v in pairs(opt) do opt[k] = tonumber(os.getenv(k)) or os.getenv(k) or opt[k] end
print(opt)
if opt.display == 0 then opt.display = false end
if opt.gpu > 0 then
   require 'cunn'
   cutorch.setDevice(opt.gpu)
end

-- set seed
if opt.manualSeed == 0 then
    opt.manualSeed = torch.random(1, 10000)
end
print("Seed: " .. opt.manualSeed)
torch.manualSeed(opt.manualSeed)
torch.setnumthreads(1)
torch.setdefaulttensortype('torch.FloatTensor')

-- create data loader
local DataLoader = paths.dofile('data/segbg/data.lua')
local data = DataLoader.new(opt.nThreads, opt)
print("Dataset Size: ", data:size())

---------------------------------------------------------------------------
-- Initialize network variables
---------------------------------------------------------------------------
local function weights_init(m)
   local name = torch.type(m)
   if name:find('Convolution') then
      m.weight:normal(0.0, 0.02)
      m.bias:fill(0)
   elseif name:find('BatchNormalization') then
      if m.weight then m.weight:normal(1.0, 0.02) end
      if m.bias then m.bias:fill(0) end
   end
end

local real_label = 1
local fake_label = 0
---------------------------------------------------------------------------
-- Generator net
---------------------------------------------------------------------------
local netG
if opt.continue_GPath then
    netG = torch.load(opt.continue_GPath)
else
    if opt.fineSize == 256 then
        netG = defineG_unet_256(opt.nc, opt.nc, opt.ngf)
    elseif opt.fineSize == 128 then
        netG = defineG_unet_128(opt.nc, opt.nc, opt.ngf)
    end
    netG:apply(weights_init)
end

---------------------------------------------------------------------------
-- Adversarial discriminator net
---------------------------------------------------------------------------
local netD
if opt.continue_DPath then
    netD = util.load(opt.continue_DPath, opt.gpu)
else
    if opt.fineSize == 256 then
        netD = defineD_256(opt.nc, opt.ndf)
    elseif opt.fineSize == 128 then
        netD = defineD_128(opt.nc, opt.ndf)
    end
    netD:apply(weights_init)
end

---------------------------------------------------------------------------
-- Loss Metrics
---------------------------------------------------------------------------
local criterion = nn.BCECriterion()
local criterionL1 = nn.SmoothL1Criterion()

---------------------------------------------------------------------------
-- Setup Solver
---------------------------------------------------------------------------
print('LR of Gen is ',(opt.wRecon>0 and opt.wRecon<1) and 10 or 1,'times Adv')
optimStateG = {
   learningRate = opt.lr,
   beta1 = opt.beta1,
   learningRateDecay = opt.lrd,
}
optimStateD = {
   learningRate = opt.lr,
   beta1 = opt.beta1,
   learningRateDecay = opt.lrd,
}

---------------------------------------------------------------------------
-- Initialize data variables
---------------------------------------------------------------------------
local input_ctx_vis = torch.Tensor(opt.batchSize, opt.nc, opt.fineSize, opt.fineSize)
local input_ctx = torch.Tensor(opt.batchSize, opt.nc, opt.fineSize, opt.fineSize)
local input_center = torch.Tensor(opt.batchSize, opt.nc, opt.fineSize, opt.fineSize)
local input_real_center = torch.Tensor(opt.batchSize, opt.nc, opt.fineSize, opt.fineSize)
local label = torch.Tensor(opt.batchSize)
local real_ctx, mask, fake
local errD, errG, errRecon
local epoch_tm = torch.Timer()
local tm = torch.Timer()
local data_tm = torch.Timer()

if pcall(require, 'cudnn') and pcall(require, 'cunn') and opt.gpu>0 then
    print('Using CUDNN !')
end
if opt.gpu > 0 then
   require 'cunn'
   cutorch.setDevice(opt.gpu)
   input_ctx_vis = input_ctx_vis:cuda(); input_ctx = input_ctx:cuda();  input_center = input_center:cuda()
   label = label:cuda()
   netD:cuda();           netG:cuda();           criterion:cuda();      
    criterionL1:cuda(); input_real_center = input_real_center:cuda();
end
print('NetG:',netG)
print('NetD:',netD)

local parametersD, gradParametersD = netD:getParameters()
local parametersG, gradParametersG = netG:getParameters()

if opt.display then disp = require 'display' end

---------------------------------------------------------------------------
-- Define generator and adversary closures
---------------------------------------------------------------------------
-- create closure to evaluate f(X) and df/dX of discriminator
local fDx = function(x)
   netD:apply(function(m) if torch.type(m):find('Convolution') then m.bias:zero() end end)
   netG:apply(function(m) if torch.type(m):find('Convolution') then m.bias:zero() end end)

   gradParametersD:zero()

   -- train with real
   data_tm:reset(); data_tm:resume()
   real_ctx, mask = data:getBatch()
   real_center = real_ctx  -- view
   input_center:copy(real_center)
   input_real_center:copy(real_center)

   real_ctx[{{},{1},{},{}}][mask] = 2*117.0/255.0 - 1.0
   real_ctx[{{},{2},{},{}}][mask] = 2*104.0/255.0 - 1.0
   real_ctx[{{},{3},{},{}}][mask] = 2*123.0/255.0 - 1.0
   input_ctx:copy(real_ctx)
   data_tm:stop()

   label:fill(real_label)
   local output = netD:forward(input_center)
   local errD_real = criterion:forward(output, label)
   local df_do = criterion:backward(output, label)
   netD:backward(input_center, df_do)
   
   -- train with fake
   fake = netG:forward(input_ctx)
   input_center:copy(fake)
   label:fill(fake_label)

   local output = netD:forward(input_center)
   local errD_fake = criterion:forward(output, label)
   local df_do = criterion:backward(output, label)
   netD:backward(input_center, df_do)

   errD = errD_real + errD_fake

   return errD, gradParametersD
end

-- create closure to evaluate f(X) and df/dX of generator
local fGx = function(x)
   netD:apply(function(m) if torch.type(m):find('Convolution') then m.bias:zero() end end)
   netG:apply(function(m) if torch.type(m):find('Convolution') then m.bias:zero() end end)

   gradParametersG:zero()

   --[[ the three lines below were already executed in fDx, so save computation
   noise:uniform(-1, 1) -- regenerate random noise
   local fake = netG:forward({input_ctx,noise})
   input_center:copy(fake) ]]--
   label:fill(real_label) -- fake labels are real for generator cost

   local output = netD.output -- netD:forward({input_ctx,input_center}) was already executed in fDx, so save computation
   errG = criterion:forward(output, label)
   local df_do = criterion:backward(output, label)
   local df_dg = netD:updateGradInput(input_center, df_do)

   local errG_total = errG
   if opt.wRecon~=0 then
      errRecon = criterionL1:forward(input_center, input_real_center)
      local df_dg_recon = criterionL1:backward(input_center, input_real_center)

      if opt.useOverlapPred==0 then
        if (opt.wRecon>0 and opt.wRecon<1) then
          df_dg:mul(1-opt.wRecon):add(opt.wRecon,df_dg_recon)
          errG_total = (1-opt.wRecon)*errG + opt.wRecon*errRecon
        else
          df_dg:add(opt.wRecon,df_dg_recon)
          errG_total = errG + opt.wRecon*errRecon
        end
      else
        local overlapWeight = 10
        local wReconMatrix = df_dg_recon:clone():fill(overlapWeight*opt.wRecon)
        for i=1,3 do
          wReconMatrix[{{},{i},{},{}}][mask] = opt.wRecon
        end
        if (opt.wRecon>0 and opt.wRecon<1) then
          df_dg:mul(1-opt.wRecon):addcmul(1,wReconMatrix,df_dg_recon)
          errG_total = (1-opt.wRecon)*errG + opt.wRecon*errRecon
        else
          df_dg:addcmul(1,wReconMatrix,df_dg_recon)
          errG_total = errG + opt.wRecon*errRecon
        end
      end
   end

   netG:backward(input_ctx, df_dg)

   return errG_total, gradParametersG
end

---------------------------------------------------------------------------
-- Train Context Encoder
---------------------------------------------------------------------------
local plot_config = {
    title = opt.name,
    labels = {"epoch", "100*err_recon", "err_G", "err_D"},
    ylabel = "loss",
}
local plot_data = {}
local nstep = math.min(math.floor(data:size()/opt.batchSize), opt.ntrain)
for epoch = opt.continue_start_iter+1, opt.continue_start_iter+opt.niter do
   collectgarbage()
   epoch_tm:reset()
   for i = 1, nstep do
      tm:reset()
      -- (1) Update D network: maximize log(D(x)) + log(1 - D(G(z)))
      optim.adam(fDx, parametersD, optimStateD)

      -- (2) Update G network: maximize log(D(G(z)))
      optim.adam(fGx, parametersG, optimStateG)

      -- display
      if i % opt.display_iter == 0 and opt.display then
          disp.image(fake, {win=opt.display_id+1, title=opt.name .. '-fake'})
          real_ctx[{{},{1},{},{}}][mask] = 1.0
          real_ctx[{{},{2},{},{}}][mask] = 1.0
          real_ctx[{{},{3},{},{}}][mask] = 1.0
          disp.image(real_ctx, {win=opt.display_id+2, title=opt.name .. '-input'})
          disp.image(input_real_center, {win=opt.display_id+3, title=opt.name .. '-real'})
          table.insert(plot_data, {(epoch-1)*nstep+i, 100*errRecon, errG, errD})
          plot_config.win = disp.plot(plot_data, plot_config) 
      end

      -- logging
      if i % 10 == 0 then
         print(('Epoch: [%d][%8d / %8d]\t Time: %.3f  DataTime: %.3f  '
                   .. '  Err_Recon: %.4f   Err_G: %.4f  Err_D: %.4f'):format(
                 epoch, i, nstep,
                 tm:time().real, data_tm:time().real, errRecon or -1,
                 errG and errG or -1, errD and errD or -1))
      end
   end
   paths.mkdir('checkpoints')
   parametersD, gradParametersD = nil, nil -- nil them to avoid spiking memory
   parametersG, gradParametersG = nil, nil
   if epoch % 20 == 0 then
       local outdir = paths.concat('checkpoints', opt.name)
      if not paths.dirp(outdir) then paths.mkdir(outdir) end
      torch.save('checkpoints/' .. opt.name .. '/' .. epoch .. '_net_G.t7', netG)  -- util.save will cause oom
      -- torch.save('checkpoints/' .. opt.name .. '/' .. epoch .. '_net_D.t7', netD)
      util.save('checkpoints/' .. opt.name .. '/' .. epoch .. '_net_D.t7', netD, opt.gpu)
      print('save checkpoint !!!')
   end
   parametersD, gradParametersD = netD:getParameters() -- reflatten the params and get them
   parametersG, gradParametersG = netG:getParameters()
   print(('End of epoch %d / %d \t Time Taken: %.3f'):format(
            epoch, opt.niter, epoch_tm:time().real))
end
