require 'image'
require 'nn'
require 'nngraph'
require 'cunn'
util = paths.dofile('util.lua')
torch.setdefaulttensortype('torch.FloatTensor')

opt = {
    batchSize = 1,        -- number of samples to produce
    net = './checkpoints/overlapSegx256/200_net_G.t7',              -- path to the generator network
    inputDir = '../image/sample/image/',
    maskDir = '../image/sample/seg',
    gpu = 1,               -- gpu mode. 0 = CPU, 1 = 1st GPU etc.
    nc = 3,                -- # of channels in input
    fineSize = 256,        -- size of random crops
    nThreads = 1,          -- # of data loading threads to use
    manualSeed = 0,        -- 0 means random seed
    wholeAsInput = 1,
}
for k,v in pairs(opt) do opt[k] = tonumber(os.getenv(k)) or os.getenv(k) or opt[k] end
print(opt)
if opt.wholeAsInput == 0 then opt.wholeAsInput = false end

-- set seed
if opt.manualSeed == 0 then
    opt.manualSeed = torch.random(1, 10000)
end
print("Seed: " .. opt.manualSeed)
torch.manualSeed(opt.manualSeed)

-- load Context-Encoder
assert(opt.net ~= '', 'provide a generator model')
net = torch.load(opt.net)
net:evaluate()

-- initialize variables
input_image_ctx = torch.Tensor(opt.batchSize, opt.nc, opt.fineSize, opt.fineSize)
-- port to GPU
if opt.gpu > 0 then
    if pcall(require, 'cudnn') then
        print('Using CUDNN !')
        require 'cudnn'
    end
    net:cuda()
    input_image_ctx = input_image_ctx:cuda()
else
   net:float()
end
print(net)

-- concat last folder's name with net basename
local pos = string.find(opt.net, "/[^/]*$")
opt.net = opt.net:sub(1, pos-1) .. '-' .. opt.net:sub(pos+1)

local predict = function(origPath, maskPath) 
    local orig = image.load(origPath, 3, 'float'):mul(2):add(-1)    -- [-1, 1]
    local mask = image.load(maskPath, 1, 'float'):round():byte():squeeze()  -- 0/1
    assert(orig:size(2)==mask:size(1) and orig:size(3)==mask:size(2))

    -- if wholeAsInput, predict for whole picture 
    if opt.wholeAsInput then
        orig = image.scale(orig, opt.fineSize, opt.fineSize)
        mask = image.scale(mask, opt.fineSize, opt.fineSize)
        local centerImage = orig:clone()
        centerImage[{{1},{},{}}][mask] = 2*117.0/255.0 - 1.0
        centerImage[{{2},{},{}}][mask] = 2*104.0/255.0 - 1.0
        centerImage[{{3},{},{}}][mask] = 2*123.0/255.0 - 1.0
        input_image_ctx[1]:copy(centerImage)
        local pred_center = net:forward(input_image_ctx)[1]
        orig:add(1):mul(0.5)
        centerImage:add(1):mul(0.5)
        pred_center:add(1):mul(0.5)

        pretty_output = torch.Tensor(4, opt.nc, opt.fineSize, opt.fineSize)
        pretty_output[1]:copy(orig)
        centerImage[{{1},{},{}}][mask] = 1.0 
        centerImage[{{2},{},{}}][mask] = 1.0
        centerImage[{{3},{},{}}][mask] = 1.0
        pretty_output[2][1]:copy(mask)
        pretty_output[2][2]:copy(mask)
        pretty_output[2][3]:copy(mask)
        pretty_output[3]:copy(pred_center)
        orig[{{1},{},{}}][mask] = pred_center[{{1},{},{}}][mask]:float()
        orig[{{2},{},{}}][mask] = pred_center[{{2},{},{}}][mask]:float()
        orig[{{3},{},{}}][mask] = pred_center[{{3},{},{}}][mask]:float()
        pretty_output[4]:copy(orig)
        local outdir = paths.concat('./testresults/seg/', string.sub(paths.basename(opt.net), 1, -4))
        if not paths.dirp(outdir) then paths.mkdir(outdir) end
        image.save(paths.concat(outdir, paths.basename(maskPath)), image.toDisplayTensor(pretty_output))
        return
    end


    local imgH, imgW = orig:size(2), orig:size(3)
    -- find the bounding box of mask, [(x0, y0), (x1, y1)], containing the bounding line
    local x = torch.max(mask, 1):squeeze()
    local y = torch.max(mask, 2):squeeze()
    local x0, y0, x1, y1
    for i=1, x:numel() do if x[i] == 1 then x0 = i break end end
    for i=1, x:numel() do if x[i] == 1 then x1 = i end end
    for i=1, y:numel() do if y[i] == 1 then y0 = i break end end
    for i=1, y:numel() do if y[i] == 1 then y1 = i end end
    
    local h, w = y1-y0+1, x1-x0+1
    local area = mask:sum()

    -- calculate scale, such that h*scale<=size, w*scale<=size, area*scale^2<=0.4*size^2
    local scale = math.min(1.0, opt.fineSize / h, opt.fineSize / w, torch.sqrt(0.4/area) * opt.fineSize)

    -- resize
    local resized_h, resized_w = torch.ceil(scale * imgH), torch.ceil(scale * imgW)
    assert(resized_h>=opt.fineSize and resized_w>=opt.fineSize) -- resized image should be larger than fineSize*fineSize
    local resized_orig = image.scale(orig, resized_w, resized_h)
    local resized_mask = image.scale(mask, resized_w, resized_h)

    -- inpaint center coordinate in resized image
    local center_x, center_y = torch.floor(scale * (x0+x1)/2), torch.floor(scale * (y0+y1)/2)
    center_x = math.min(math.max(center_x, opt.fineSize/2), resized_w-opt.fineSize/2)
    center_y = math.min(math.max(center_y, opt.fineSize/2), resized_h-opt.fineSize/2)

    -- print(center_x, center_y,resized_w, resized_h)

    -- construct input to the inpainting net
    local centerImage = resized_orig[{{}, {center_y-opt.fineSize/2+1, center_y+opt.fineSize/2}, {center_x-opt.fineSize/2+1, center_x+opt.fineSize/2}}]:clone()
    local centerMask = resized_mask[{{center_y-opt.fineSize/2+1, center_y+opt.fineSize/2}, {center_x-opt.fineSize/2+1, center_x+opt.fineSize/2}}]:clone()
    -- print(centerMask, centerMask:sum())
    
    centerImage[{{1},{},{}}][centerMask] = 2*117.0/255.0 - 1.0
    centerImage[{{2},{},{}}][centerMask] = 2*104.0/255.0 - 1.0
    centerImage[{{3},{},{}}][centerMask] = 2*123.0/255.0 - 1.0

    -- predict
    local pred_center
    input_image_ctx[1]:copy(centerImage)
    if opt.noiseGen then
        pred_center = net:forward({input_image_ctx,noise})
    else
        pred_center = net:forward(input_image_ctx)
    end

    -- reconstruct origin image
    local resized_recon = resized_orig:clone()
    resized_recon[{{}, {center_y-opt.fineSize/2+1, center_y+opt.fineSize/2}, {center_x-opt.fineSize/2+1, center_x+opt.fineSize/2}}]:copy(pred_center[{{1},{},{},{}}])
    local orig_sized_recon = image.scale(resized_recon, imgW, imgH)
    local final_recon = orig:clone()
    final_recon[{{1},{},{}}][mask] = orig_sized_recon[{{1},{},{}}][mask]:float()
    final_recon[{{2},{},{}}][mask] = orig_sized_recon[{{2},{},{}}][mask]:float()
    final_recon[{{3},{},{}}][mask] = orig_sized_recon[{{3},{},{}}][mask]:float()
    local orig_input = orig:clone()
    orig_input[{{1},{},{}}][mask] = 1.0
    orig_input[{{2},{},{}}][mask] = 1.0
    orig_input[{{3},{},{}}][mask] = 1.0

    -- re-transform pixel value to normal
    orig:add(1):mul(0.5)
    final_recon:add(1):mul(0.5)
    orig_input:add(1):mul(0.5)

    -- save
    pretty_output = torch.Tensor(3, opt.nc, imgH, imgW)
    pretty_output[1]:copy(orig)
    pretty_output[2]:copy(orig_input)
    pretty_output[3]:copy(final_recon)
    local outdir = paths.concat('./testresults/seg/', string.sub(paths.basename(opt.net), 1, -4) .. '-onlymask')
    if not paths.dirp(outdir) then paths.mkdir(outdir) end
    image.save(paths.concat(outdir, paths.basename(maskPath)), image.toDisplayTensor(pretty_output))
end


local maskDict = {}
if opt.maskDir~='' then
    for file in paths.iterfiles(opt.maskDir) do
        maskDict[string.sub(file, 1, string.find(file, '-')-1)] = paths.concat(opt.maskDir, file) -- preserve only one mask
    end
end

if opt.wholeAsInput then
    print('inpaint in the whole image .......')
else
    print('inpaint only in the given mask region .......')
end

for file in paths.iterfiles(opt.inputDir) do
    local origPath = paths.concat(opt.inputDir, file)
    local maskPath = maskDict[string.sub(file, 1, -5)]
    predict(origPath, maskPath)
end

