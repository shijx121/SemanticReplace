# coding: utf-8
import cv2
import numpy as np
import argparse
import os

def edit(filename):
    img = cv2.imread(filename)
    h, w, c = img.shape
    back = img[:, :w/4, :]
    fore = img[:, w/2:w/2+w/4, :]
    mask = cv2.cvtColor(img[:, w/4:w/2, :], cv2.COLOR_BGR2GRAY)
    mask = cv2.dilate(mask, np.ones((11, 11), 'uint8')) # necessary to dilate!
    minx, miny, maxx, maxy = w, h, 0, 0
    for i in range(h):
        for j in range(w/4):
            if mask[i,j] > 50:
                mask[i, j] = 255
                minx = min(minx, j)
                maxx = max(maxx, j)
                miny = min(miny, i)
                maxy = max(maxy, i)
    center = ((minx+maxx)/2, (miny+maxy)/2)

    out = cv2.seamlessClone(fore, back, mask, center, cv2.NORMAL_CLONE)
    return np.concatenate((img, out), axis=1)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--input', type=str, required=True, help='result folder of test_seg.lua')
    parser.add_argument('--output', type=str)
    args = parser.parse_args()

    if not os.path.isdir(args.input):
        raise Exception('input dir not exist')
    if not args.output:
        args.output = args.input + '_pie'
    if not os.path.exists(args.output):
        os.makedirs(args.output)

    for f in os.listdir(args.input):
        out_img = edit(os.path.join(args.input, f))
        out_file = os.path.join(args.output, os.path.basename(f))
        cv2.imwrite(out_file, out_img)

if __name__ == '__main__':
    main()

