require 'torch'
require 'nn'
require 'optim'
require 'image'
require 'models'
require 'nngraph'
require 'cunn'
require 'cudnn'
require 'detection'
json = require 'cjson'
disp = require 'display'
util = paths.dofile('util.lua')

opt = {
    cocoDir = '/data/mscoco',
    subCatsJson = './data/cocoObj/subCats.json',
    featNormJson = './data/cocoObj/featNorm.json',
    inpaintNetPath = './checkpoints/Segx256_no-overlap_no-aug-Glr/300_net_G.t7',
    frcnnModelPath = './FRCNN.lua',
    frcnnWeightPath = './checkpoints/frcnn_vgg16_mscoco_iter_200000_class_10.t7',
   batchSize = 1,         -- number of samples to produce
   fineSize = 256,         -- size of random crops. Only 256 supported.
   ngf = 64,               -- #  of gen filters in first conv layer
   ndf = 64,               -- #  of discrim filters in first conv layer
   nc = 3,                 -- # of channels in input
   nfeat = 128,            -- # of features
   nclass = 10,
   wRecon = 1,
   wAdv = 1e-3,
   wCls = 1e-4,
   wFeat = 1e-5,
   focus = 1,             -- recon weight in the bbox
   nThreads = 4,           -- #  of data loading threads to use
   niter = 500000,             -- #  of iter at starting learning rate
   lr = 0.002,            -- initial learning rate for adam
   lrd = 5e-5,             -- learning rate decay, make lr multiply 1/11 after 2e5 updates
   beta1 = 0.5,            -- momentum term of adam
   display_id = 10,        -- display window id.
   gpu = 1,                -- gpu=X is GPU mode on GPU X
   name = 'ROI-replace',        -- name of the experiment you are running
   manualSeed = 1234,         -- 0 means random seed
   sameProb = 1,

   dnoise = true,
   label_switch = false,
   debug=false,
}
for k,v in pairs(opt) do opt[k] = tonumber(os.getenv(k)) or os.getenv(k) or opt[k] end
print(opt)
if opt.display == 0 then opt.display = false end
cutorch.setDevice(opt.gpu)


-- set seed
if opt.manualSeed == 0 then
    opt.manualSeed = torch.random(1, 10000)
end
print("Seed: " .. opt.manualSeed)
torch.manualSeed(opt.manualSeed)
torch.setnumthreads(1)
torch.setdefaulttensortype('torch.FloatTensor')

-- create data loader
local DataLoader = paths.dofile('data/cocoObj/data.lua')
local data = DataLoader.new(opt.nThreads, opt)
print("Dataset Size: ", data:size())

---------------------------------------------------------------------------
-- Initialize network variables
---------------------------------------------------------------------------
local function weights_init(m)
   local name = torch.type(m)
   if name:find('Convolution') then
      m.weight:normal(0.0, 0.02)
      m.bias:fill(0)
   elseif name:find('BatchNormalization') then
      if m.weight then m.weight:normal(1.0, 0.02) end
      if m.bias then m.bias:fill(0) end
   end
end

local real_label = 1
local fake_label = 0

---------------------------------------------------------------------------
-- load inpaint net
---------------------------------------------------------------------------
local netInpaint = torch.load(opt.inpaintNetPath)
netInpaint:apply(function(m) if m.accGradParameters then m.accGradParameters = function() end; m.gradWeight = nil; m.gradBias = nil; end end)
netInpaint:evaluate()
netInpaint:cuda()
print('inpaint net loaded.')

---------------------------------------------------------------------------
-- load weight from pretrained model 
---------------------------------------------------------------------------
function loadWeight(net, loaded)
    local model_modules = net:listModules()
    local loaded_modules = loaded:listModules()
    for i=1, #loaded_modules do
        local copy_name = loaded_modules[i].name
        if copy_name then
            for j=1, #model_modules do
                local my_name = model_modules[j].name
                if my_name and my_name == copy_name then
                    print('Copying weights from ' .. my_name .. ' layer!')
                    model_modules[j].weight:copy(loaded_modules[i].weight)
                    model_modules[j].bias:copy(loaded_modules[i].bias)
                end
            end
        end
    end
end

---------------------------------------------------------------------------
-- normalize feature for input_of_G_feat 
---------------------------------------------------------------------------
local _ = json.decode(io.open(opt.featNormJson, 'r'):read())
local feat_mean, feat_std = torch.Tensor(_.mean), torch.sqrt(torch.Tensor(_.var))

function featnorm(f)
    f = f:float()
    return torch.cdiv(f-feat_mean, feat_std)
end

---------------------------------------------------------------------------
-- domain adaption 
---------------------------------------------------------------------------
function prep(im)
    -- input: rgb, [-1,1], 3xhxw. Note im has no batch dimension!
    -- output: bgr, [0,255] and subtract mean
    local im = torch.squeeze(im)
    local im_out = im:clone()
    im_out[1] = im[3]; im_out[3] = im[1];
    im_out:add(1):mul(0.5*255)
    im_out[1]:add(-102.9801)
    im_out[2]:add(-115.9465)
    im_out[3]:add(-122.7717)
    return im_out
end

function bpprep(df)
    -- propogate gradient from output of prep4feat to input
    -- Note df has no batch dimension!
    local df = torch.squeeze(df)
    local df_out = df:clone()
    df_out[1], df_out[3] = df[3], df[1]
    df_out:mul(0.5*255)
    return df_out
end

---------------------------------------------------------------------------
-- Generator/blend net
---------------------------------------------------------------------------
local netG
netG = Decoder_ROI_replace_deeper_Ufusion(opt.nc, opt.ngf, opt.debug) -- TODO architecture
netG:apply(weights_init)
netG:cuda()

---------------------------------------------------------------------------
-- feature net
---------------------------------------------------------------------------
local netF = dofile(opt.frcnnModelPath)(opt)
local loaded = torch.load(opt.frcnnWeightPath)
loadWeight(netF, loaded)
loaded = nil
netF:evaluate()
collectgarbage()
print('frcnn netF loaded')

---------------------------------------------------------------------------
-- Adversarial discriminator net
---------------------------------------------------------------------------
local netD = defineD_256_ROI(opt.nc, opt.ndf)
netD:apply(weights_init)
netD:cuda()

print('NetG:',netG)
print('netF:',netF)
print('NetD:',netD)

local parametersD, gradParametersD = netD:getParameters()
local parametersG, gradParametersG = netG:getParameters()

---------------------------------------------------------------------------
-- Loss Metrics
---------------------------------------------------------------------------
local criterionAdv = nn.BCECriterion()
local criterionCls = nn.CrossEntropyCriterion()
local criterionL1 = nn.SmoothL1Criterion()
criterionCls:cuda();
criterionAdv:cuda();      
criterionL1:cuda();

---------------------------------------------------------------------------
-- Setup Solver
---------------------------------------------------------------------------
optimStateG = {
   learningRate = opt.lr,
   beta1 = opt.beta1,
   learningRateDecay = opt.lrd,
}
optimStateD = {
   learningRate = opt.lr*0.1,
   beta1 = opt.beta1,
   learningRateDecay = opt.lrd,
}

---------------------------------------------------------------------------
-- Initialize data variables
---------------------------------------------------------------------------
local label = torch.Tensor(opt.batchSize):cuda()
local input_of_inpaint = torch.Tensor(opt.batchSize, 3, opt.fineSize, opt.fineSize):cuda()
local input_of_G_img = torch.Tensor(opt.batchSize, 3, opt.fineSize, opt.fineSize):cuda()
local origin_holder = torch.Tensor(opt.batchSize, 3, opt.fineSize, opt.fineSize):cuda()
local fake_holder = torch.Tensor(opt.batchSize, 3, opt.fineSize, opt.fineSize):cuda()
local featA = torch.Tensor(opt.batchSize, 512, 16, 16):cuda()
local featB = torch.Tensor(opt.batchSize, 512, 16, 16):cuda()
local input_of_D_img = torch.Tensor(opt.batchSize, 3, opt.fineSize, opt.fineSize):cuda()
local D_noise = torch.Tensor(opt.batchSize, 3, opt.fineSize, opt.fineSize):cuda()
local input_of_F_img = torch.Tensor(opt.batchSize, 3, opt.fineSize, opt.fineSize):cuda()
local bboxA = torch.Tensor(1, 5):cuda()
local bboxB = torch.Tensor(1, 5):cuda()
local output_of_D_feat = torch.Tensor(opt.batchSize, opt.nfeat):zero():cuda()
local errD, errGAdv, errRecon, errFeat, errCls
local tm = torch.Timer()
local data_tm = torch.Timer()

local imA, maskA, clsA, x1A,y1A,x2A,y2A, imB, maskB, clsB, x1B,y1B,x2B,y2B, im_holder, _
local inpaint_out, semantic_featB, roi_featB, featFakeB, fake
local same=true  -- true means imB equals to imA and recon loss for whole. false means recon loss outside bbox.

---------------------------------------------------------------------------
-- Define generator and adversary closures
---------------------------------------------------------------------------
-- create closure to evaluate f(X) and df/dX of discriminator
local fDx = function(x)
   netD:apply(function(m) if torch.type(m):find('Convolution') then m.bias:zero() end end)
   netG:apply(function(m) if torch.type(m):find('Convolution') then m.bias:zero() end end)

   gradParametersD:zero()

   -- fetch data
   data_tm:reset(); data_tm:resume()
   imA, maskA, clsA, x1A, y1A, x2A, y2A = data:getBatch()
   if same then
      imB, maskB, clsB, x1B, y1B, x2B, y2B = imA, maskA, clsA, x1A, y1A, x2A, y2A
   else
      imB, maskB, clsB, x1B, y1B, x2B, y2B = data:getBatch()
   end
   data_tm:stop()
   bboxA:copy(torch.Tensor({{1,x1A,y1A,x2A,y2A}}))
   bboxB:copy(torch.Tensor({{1,x1B,y1B,x2B,y2B}}))

   -- input to the netInpaint: pollute object A in imA
   im_holder = imA:clone()
   im_holder[{{1},{},{}}][maskA] = 2*117.0/255.0 - 1.0
   im_holder[{{2},{},{}}][maskA] = 2*104.0/255.0 - 1.0
   im_holder[{{3},{},{}}][maskA] = 2*123.0/255.0 - 1.0

   -- netInpaint forward: inpaint object A
   input_of_inpaint:copy(im_holder)
   inpaint_out = netInpaint:forward(input_of_inpaint)

   -- prepare imB and get featB
   im_holder = prep(imB)
   input_of_F_img:copy(im_holder)
   _ = netF:forward({input_of_F_img, bboxB})
   semantic_featB = _[2]:clone()
   featB:copy(_[3]) -- 512 x 16 x 16

    -- prepare inpainted imA and get featA (background) 
   im_holder = prep(inpaint_out)
   input_of_F_img:copy(im_holder)
   _ = netF:forward({input_of_F_img, bboxA})
   featA:copy(_[3]) -- 512 x 16 x 16

   ------------
   --construct input of G. NOTE: input of G must be normalized to avoid gradient vanishment TODO
   ------------
   -- inpaint output
   input_of_G_img:copy(inpaint_out)
   featB:div(128)
   featA:div(128)
   ------------
   
   ------------
   -- netG forward: get fake, blend object B at the position of bbox A
   if opt.debug then
       _ = netG:forward({input_of_G_img, featA, bboxA, featB, bboxB})
       fake = _[1]
       print(torch.squeeze(_[2])[{{1}}])
       print(torch.squeeze(_[3])[{{1}}])
       print(torch.squeeze(_[4])[{{1}}])
       print('-------------------------------------------')
   else
       fake = netG:forward({input_of_G_img, featA, bboxA, featB, bboxB})
   end
   ------------

   -- train with real: imB and objectB
   label:fill(real_label)
   input_of_D_img:copy(imB)
   if opt.dnoise then
      D_noise:uniform(-2.0/255, 2.0/255)
      input_of_D_img:add(D_noise)
   end
   local output = netD:forward({input_of_D_img, bboxB})
   local errD_real = criterionAdv:forward(output, label)
   if type(errD_real)~='number' then errD_real = errD_real:clone() end
   local df_do_real = criterionAdv:backward(output, label)
   netD:backward({input_of_D_img, bboxB}, df_do_real)

   if opt.label_switch then
       netD:backward({input_of_D_img, bboxB}, df_do_real)
       label:fill(fake_label)
       criterionAdv:forward(output, label)
       df_do_real = criterionAdv:backward(output, label)
       netD:backward({input_of_D_img, bboxB}, df_do_real)
   end
   
   -- train with fake: generated fake based on imA, and bbox of objectA
   label:fill(fake_label)
   input_of_D_img:copy(fake)
   local output = netD:forward({input_of_D_img, bboxA})
   local errD_fake = criterionAdv:forward(output, label)
   local df_do_fake = criterionAdv:backward(output, label)
   netD:backward({input_of_D_img, bboxA}, df_do_fake)

   if opt.label_switch then
       netD:backward({input_of_D_img, bboxA}, df_do_fake)
       label:fill(real_label)
       criterionAdv:forward(output, label)
       df_do_fake = criterionAdv:backward(output, label)
       netD:backward({input_of_D_img, bboxA}, df_do_fake)
   end

   errD = errD_real + errD_fake

   return errD, gradParametersD
end

-- create closure to evaluate f(X) and df/dX of generator
local fGx = function(x)
   netD:apply(function(m) if torch.type(m):find('Convolution') then m.bias:zero() end end)
   netG:apply(function(m) if torch.type(m):find('Convolution') then m.bias:zero() end end)

   gradParametersG:zero()

   local errG_total
   local df_dg_total
   -------------
   -- adv loss
   -------------
   label:fill(real_label)
   local output = netD.output
   errGAdv = criterionAdv:forward(output, label)
   local df_do = criterionAdv:backward(output, label)
   df_do:mul(opt.wAdv)
   local df_dg_adv = netD:updateGradInput({input_of_D_img, bboxA}, df_do)[1]

   ------------
   -- semantic feat loss and classification loss of generated fake object B at bbox A
   -----------
   -- input to the netF: prepare fake and bbox A and class B
   im_holder = prep(fake)
   input_of_F_img:copy(im_holder)
   -- netF forward: get fake B's feature
   local output = netF:forward({input_of_F_img, bboxA})
   errCls = criterionCls:forward(output[1], clsB + 1)
   local df_do_cls = criterionCls:backward(output[1], clsB + 1)
   errFeat = criterionL1:forward(output[2], semantic_featB)
   if type(errFeat)~='number' then errFeat = errFeat:clone() end
   local df_do_feat = criterionL1:backward(output[2], semantic_featB):clone()

   df_do_cls:mul(opt.wCls);
   df_do_feat:mul(opt.wFeat); -- multiply corresponding weight
   local df_dg_fromF = netF:updateGradInput({input_of_F_img, bboxA}, {df_do_cls, df_do_feat, output[3]:clone():zero()})[1]
   df_dg_fromF = bpprep(df_dg_fromF)
   df_dg_fromF = df_dg_fromF:view(1, 3, opt.fineSize, opt.fineSize)

   -------------
   -- recon loss outside bbox A
   -------------
   origin_holder:copy(imA)
   fake_holder:copy(fake)
   if not same then
      origin_holder[{{},{},{y1A,y2A},{x1A,x2A}}]=0
      fake_holder[{{},{},{y1A,y2A},{x1A,x2A}}]=0
   end
   errRecon = criterionL1:forward(fake_holder, origin_holder) 
   local df_dg_recon = criterionL1:backward(fake_holder, origin_holder):clone()
   df_dg_recon:mul(opt.wRecon)
   df_dg_recon[{{},{},{y1A,y2A},{x1A,x2A}}]:mul(opt.focus)  -- give a large recovery weight to the target object

   -------------
   -- total error and grad
   -------------
   errG_total = opt.wAdv*errGAdv + opt.wRecon*errRecon + opt.wFeat*errFeat + opt.wCls*errCls
   df_dg_total = df_dg_fromF:add(df_dg_recon):add(df_dg_adv)
 
   if opt.debug then
      netG:backward({input_of_G_img, featA, bboxA, featB, bboxB}, {df_dg_total, _[2]:clone():zero(), _[3]:clone():zero(), _[4]:clone():zero()})
   else
      netG:backward({input_of_G_img, featA, bboxA, featB, bboxB}, df_dg_total)
   end

   return errG_total, gradParametersG
end

---------------------------------------------------------------------------
-- Train Context Encoder
---------------------------------------------------------------------------
local plot_config = {
    title = opt.name,
    labels = {"iter", "100*err_recon", "err_feat", "err_cls", "err_Adv", "errD"},
    -- labels = {"iter", "200*err_recon", "err_Adv", "errD"},
    ylabel = "loss",
}
local plot_data = {}
for i = 1, opt.niter do
  tm:reset(); tm:resume()
  -- randomly decide whether same=ture at this step
  same = math.random() <= opt.sameProb 
  -- (1) Update D network: maximize log(D(x)) + log(1 - D(G(z)))
  optim.adam(fDx, parametersD, optimStateD)
  -- (2) Update G network: maximize log(D(G(z)))
  optim.adam(fGx, parametersG, optimStateG)
  tm:stop()

  -- display: [-1,1] to [0,255] and draw bbox
  if i % 50 == 0 then
      imA:add(1):mul(0.5*255); imA = image.drawRect(imA, x1A, y1A, x2A, y2A, {lineWidth=5,color={255,0,0}})
      disp.image(imA, {win=opt.display_id, title=opt.name .. '-A'})

      fake=fake:add(1):mul(0.5*255):float():squeeze()
      fake = image.drawRect(fake, x1A, y1A, x2A, y2A, {lineWidth=5,color={255,0,0}})
      disp.image(fake, {win=opt.display_id+1, title=opt.name .. '-fake'})

      inpaint_out=inpaint_out:add(1):mul(0.5*255):float():squeeze()
      inpaint_out = image.drawRect(inpaint_out, x1A, y1A, x2A, y2A, {lineWidth=5,color={255,0,0}})
      disp.image(inpaint_out, {win=opt.display_id+2, title=opt.name .. '-inpaint'})

      imB:add(1):mul(0.5*255); imB = image.drawRect(imB, x1B, y1B, x2B, y2B, {lineWidth=5,color={255,0,0}})
      disp.image(imB, {win=opt.display_id+3, title=opt.name .. '-B'})
      
      table.insert(plot_data, {i, 100*errRecon, errFeat, errCls, errGAdv, errD})
      -- table.insert(plot_data, {i, 200*errRecon,  errGAdv, errD})
      plot_config.win = disp.plot(plot_data, plot_config) 
  end

  -- logging
  if i % 20 == 0 then
     collectgarbage()
     print(('step: [%7d/%7d] Time: %.3f DataTime: %.3f\t'
               .. 'Recon:%.4f   Feat:%.2f   Cls:%.2f   GAdv:%.2f   DAdv:%.2f'):format(
             i, opt.niter, tm:time().real, data_tm:time().real, 
             errRecon or -1, errFeat or -1, errCls or -1, errGAdv or -1, errD or -1))
  end
   parametersD, gradParametersD = nil, nil -- nil them to avoid spiking memory
   parametersG, gradParametersG = nil, nil
   if i % 50000 == 0 then
       local outdir = paths.concat('checkpoints', opt.name)
      if not paths.dirp(outdir) then paths.mkdir(outdir) end
      torch.save('checkpoints/' .. opt.name .. '/' .. i .. '_net_G.t7', netG)  -- util.save will cause oom due to gmodule
      torch.save('checkpoints/' .. opt.name .. '/' .. i .. '_net_D.t7', netD)
      print('save checkpoint' .. i)
   end
   parametersD, gradParametersD = netD:getParameters() -- reflatten the params and get them
   parametersG, gradParametersG = netG:getParameters()
end
