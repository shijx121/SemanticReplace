require 'image'
require 'nn'
util = paths.dofile('util.lua')
torch.setdefaulttensortype('torch.FloatTensor')

opt = {
    batchSize = 1,        -- number of samples to produce
    net = './checkpoints/seg-with-obj_100_net_G.t7',              -- path to the generator network
    full = 0,              -- 1 means net basename is full, 0 means net basename is not enough
    inputDir = '../image/sample/image/',
    maskDir = '../image/sample/seg/',
    gpu = 1,               -- gpu mode. 0 = CPU, 1 = 1st GPU etc.
    nc = 3,                -- # of channels in input
    display = 0,           -- Display image: 0 = false, 1 = true
    loadSize = 0,          -- resize the loaded image to loadsize maintaining aspect ratio. 0 means don't resize. -1 means scale randomly between [0.5,2] -- see donkey_folder.lua
    fineSize = 128,        -- size of random crops
    nThreads = 1,          -- # of data loading threads to use
    manualSeed = 0,        -- 0 means random seed
    useOverlapPred = 0,        -- overlapping edges (1 means yes, 0 means no). 1 means put 10x more L2 weight on unmasked region.
    wholeAsInput = 1,       -- 1 means whole as input, 0 means no

    -- Extra Options:
    noiseGen = 0,          -- 0 means false else true; only works if network was trained with noise too.
    noisetype = 'normal',  -- type of noise distribution (uniform / normal)
    nz = 100,              -- length of noise vector if used
}
for k,v in pairs(opt) do opt[k] = tonumber(os.getenv(k)) or os.getenv(k) or opt[k] end
print(opt)
if opt.display == 0 then opt.display = false end
if opt.noiseGen == 0 then opt.noiseGen = false end
if opt.wholeAsInput == 0 then opt.wholeAsInput = false end
if opt.full == 0 then opt.full = false end

-- set seed
if opt.manualSeed == 0 then
    opt.manualSeed = torch.random(1, 10000)
end
print("Seed: " .. opt.manualSeed)
torch.manualSeed(opt.manualSeed)

-- load Context-Encoder
assert(opt.net ~= '', 'provide a generator model')
net = util.load(opt.net, opt.gpu)
net:evaluate()

-- initialize variables
input_image_ctx = torch.Tensor(opt.batchSize, opt.nc, opt.fineSize, opt.fineSize)
local noise 
if opt.noiseGen then
    noise = torch.Tensor(opt.batchSize, opt.nz, 1, 1)
    if opt.noisetype == 'uniform' then
        noise:uniform(-1, 1)
    elseif opt.noisetype == 'normal' then
        noise:normal(0, 1)
    end
end

-- port to GPU
if opt.gpu > 0 then
    require 'cunn'
    if pcall(require, 'cudnn') then
        print('Using CUDNN !')
        require 'cudnn'
        net = util.cudnn(net)
    end
    net:cuda()
    input_image_ctx = input_image_ctx:cuda()
    if opt.noiseGen then
        noise = noise:cuda()
    end
else
   net:float()
end
print(net)


if not opt.full then  -- concat last folder's name with net basename
    local pos = string.find(opt.net, "/[^/]*$")
    opt.net = opt.net:sub(1, pos-1) .. '-' .. opt.net:sub(pos+1)
end


local predict = function(origPath, maskPath) 
    local orig = image.load(origPath, 3, 'float'):mul(2):add(-1)    -- [-1, 1]

    -- if wholeAsInput, predict immediately and maskPath doesn't matter. 
    if opt.wholeAsInput then
        orig = image.scale(orig, opt.fineSize, opt.fineSize)
        input_image_ctx[1]:copy(orig)
        local pred_center = net:forward(input_image_ctx)
        orig:add(1):mul(0.5)
        pred_center:add(1):mul(0.5)
        pretty_output = torch.Tensor(2, opt.nc, opt.fineSize, opt.fineSize)
        pretty_output[1]:copy(orig)
        pretty_output[2]:copy(pred_center)
        local outdir = paths.concat('./testresults/objseg/', string.sub(paths.basename(opt.net), 1, -4))
        if not paths.dirp(outdir) then paths.mkdir(outdir) end
        image.save(paths.concat(outdir, paths.basename(origPath)), image.toDisplayTensor(pretty_output))
        return
    end


    local mask = image.load(maskPath, 1, 'float'):round():byte():squeeze()  -- 0/1
    assert(orig:size(2)==mask:size(1) and orig:size(3)==mask:size(2))
    local imgH, imgW = orig:size(2), orig:size(3)
    -- find the bounding box of mask, [(x0, y0), (x1, y1)], containing the bounding line
    local x = torch.max(mask, 1):squeeze()
    local y = torch.max(mask, 2):squeeze()
    local x0, y0, x1, y1
    for i=1, x:numel() do if x[i] == 1 then x0 = i break end end
    for i=1, x:numel() do if x[i] == 1 then x1 = i end end
    for i=1, y:numel() do if y[i] == 1 then y0 = i break end end
    for i=1, y:numel() do if y[i] == 1 then y1 = i end end
    
    local h, w = y1-y0+1, x1-x0+1
    local area = mask:sum()

    -- calculate scale, such that h*scale<=size, w*scale<=size, area*scale^2<=0.4*size^2
    local scale = math.min(1.0, opt.fineSize / h, opt.fineSize / w, torch.sqrt(0.4/area) * opt.fineSize)

    -- resize
    local resized_h, resized_w = torch.ceil(scale * imgH), torch.ceil(scale * imgW)
    assert(resized_h>=opt.fineSize and resized_w>=opt.fineSize) -- resized image should be larger than fineSize*fineSize
    local resized_orig = image.scale(orig, resized_w, resized_h)
    local resized_mask = image.scale(mask, resized_w, resized_h)

    -- inpaint center coordinate in resized image
    local center_x, center_y = torch.floor(scale * (x0+x1)/2), torch.floor(scale * (y0+y1)/2)
    center_x = math.min(math.max(center_x, opt.fineSize/2), resized_w-opt.fineSize/2)
    center_y = math.min(math.max(center_y, opt.fineSize/2), resized_h-opt.fineSize/2)

    -- print(center_x, center_y,resized_w, resized_h)

    -- construct input to the inpainting net
    local centerImage = resized_orig[{{}, {center_y-opt.fineSize/2+1, center_y+opt.fineSize/2}, {center_x-opt.fineSize/2+1, center_x+opt.fineSize/2}}]:clone()
    local centerMask = resized_mask[{{center_y-opt.fineSize/2+1, center_y+opt.fineSize/2}, {center_x-opt.fineSize/2+1, center_x+opt.fineSize/2}}]:clone()
    -- print(centerMask, centerMask:sum())
    
    -- predict
    local pred_center
    input_image_ctx[1]:copy(centerImage)
    if opt.noiseGen then
        pred_center = net:forward({input_image_ctx,noise})
    else
        pred_center = net:forward(input_image_ctx)
    end

    -- reconstruct origin image
    local resized_recon = resized_orig:clone()
    resized_recon[{{}, {center_y-opt.fineSize/2+1, center_y+opt.fineSize/2}, {center_x-opt.fineSize/2+1, center_x+opt.fineSize/2}}]:copy(pred_center[{{1},{},{},{}}])
    local orig_sized_recon = image.scale(resized_recon, imgW, imgH)
    local final_recon = orig_sized_recon:clone()

    -- re-transform pixel value to normal
    orig:add(1):mul(0.5)
    final_recon:add(1):mul(0.5)

    -- save
    pretty_output = torch.Tensor(2, opt.nc, imgH, imgW)
    pretty_output[1]:copy(orig)
    pretty_output[2]:copy(final_recon)
    local outdir = paths.concat('./testresults/objseg/', string.sub(paths.basename(opt.net), 1, -4) .. '-onlymask')
    if not paths.dirp(outdir) then paths.mkdir(outdir) end
    image.save(paths.concat(outdir, paths.basename(maskPath)), image.toDisplayTensor(pretty_output))
end

local maskDict = {}
if opt.maskDir~='' and not opt.wholeAsInput then
    print('inpaint only in the given mask region .......')
    for file in paths.iterfiles(opt.maskDir) do
        maskDict[string.sub(file, 1, string.find(file, '-')-1)] = paths.concat(opt.maskDir, file) -- preserve only one mask
    end
else
    print('inpaint in the whole image .......')
end

for file in paths.iterfiles(opt.inputDir) do
    local origPath = paths.concat(opt.inputDir, file)
    local maskPath = maskDict[string.sub(file, 1, -5)]
    predict(origPath, maskPath)
end

