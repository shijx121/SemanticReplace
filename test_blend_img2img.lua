require 'image'
require 'nn'
require 'nngraph'
util = paths.dofile('util.lua')
torch.setdefaulttensortype('torch.FloatTensor')

opt = {
    batchSize = 1,
    net = './checkpoints/blend_img2img/200_net_G.t7',              -- path to the generator network
    inputDir = '../image/sample/blend-image/',
    maskDir = '../image/sample/blend-seg/',
    gpu = 1,               -- gpu mode. 0 = CPU, 1 = 1st GPU etc.
    nc = 3,                -- # of channels in input
    fineSize = 128,        -- size of random crops
    manualSeed = 0,        -- 0 means random seed
    wholeAsInput = 1,      -- 0 means only predict for the seg bounding box and maskDir is needed
}
for k,v in pairs(opt) do opt[k] = tonumber(os.getenv(k)) or os.getenv(k) or opt[k] end
print(opt)
if opt.wholeAsInput == 0 then opt.wholeAsInput = false end

-- set seed
if opt.manualSeed == 0 then
    opt.manualSeed = torch.random(1, 10000)
end
print("Seed: " .. opt.manualSeed)
torch.manualSeed(opt.manualSeed)

-- load generator 
assert(opt.net ~= '', 'provide a generator model')
net = util.load(opt.net, opt.gpu)
net:evaluate()

-- initialize variables
input_image_ctx = torch.Tensor(opt.batchSize, opt.nc, opt.fineSize, opt.fineSize)

-- port to GPU
if opt.gpu > 0 then
    require 'cunn'
    if pcall(require, 'cudnn') then
        print('Using CUDNN !')
        require 'cudnn'
    end
    net:cuda()
    input_image_ctx = input_image_ctx:cuda()
else
   net:float()
end
print(net)

-- concat last folder's name with net basename
local pos = string.find(opt.net, "/[^/]*$")
opt.net = opt.net:sub(1, pos-1) .. '-' .. opt.net:sub(pos+1)

local outbase = './testresults/blend_img2img/'

local predict = function(origPath, maskPath) 
    local orig = image.load(origPath, 3, 'float'):mul(2):add(-1)    -- [-1, 1]

    -- if wholeAsInput, predict immediately and maskPath doesn't matter. 
    if opt.wholeAsInput then
        orig = image.scale(orig, opt.fineSize, opt.fineSize)
        input_image_ctx[1]:copy(orig)
        local delta = net:forward(input_image_ctx):float()
        local pred_center = torch.add(orig, delta)
        pred_center[pred_center:gt(1.0)] = 1.0
        pred_center[pred_center:lt(-1.0)] = -1.0
        
        orig:add(1):mul(0.5)
        pred_center:add(1):mul(0.5)
        delta:abs():mul(-1):add(1)
        pretty_output = torch.Tensor(3, opt.nc, opt.fineSize, opt.fineSize)
        pretty_output[1]:copy(orig)
        pretty_output[2]:copy(pred_center)
        pretty_output[3]:copy(delta)
        local outdir = paths.concat(outbase, string.sub(paths.basename(opt.net), 1, -4))
        if not paths.dirp(outdir) then paths.mkdir(outdir) end
        image.save(paths.concat(outdir, paths.basename(origPath)), image.toDisplayTensor(pretty_output))
        return
    end


    local mask = image.load(maskPath, 1, 'float'):round():byte():squeeze()  -- 0/1
    assert(orig:size(2)==mask:size(1) and orig:size(3)==mask:size(2))
    local imgH, imgW = orig:size(2), orig:size(3)
    -- find the bounding box of mask, [(x0, y0), (x1, y1)], containing the bounding line
    local x = torch.max(mask, 1):squeeze()
    local y = torch.max(mask, 2):squeeze()
    local x0, y0, x1, y1
    for i=1, x:numel() do if x[i] == 1 then x0 = i break end end
    for i=1, x:numel() do if x[i] == 1 then x1 = i end end
    for i=1, y:numel() do if y[i] == 1 then y0 = i break end end
    for i=1, y:numel() do if y[i] == 1 then y1 = i end end
    
    local h, w = y1-y0+1, x1-x0+1
    local area = mask:sum()

    -- calculate scale, such that h*scale<=size, w*scale<=size, area*scale^2<=0.4*size^2
    local scale = math.min(1.0, opt.fineSize / h, opt.fineSize / w, torch.sqrt(0.4/area) * opt.fineSize)

    -- resize
    local resized_h, resized_w = torch.ceil(scale * imgH), torch.ceil(scale * imgW)
    assert(resized_h>=opt.fineSize and resized_w>=opt.fineSize) -- resized image should be larger than fineSize*fineSize
    local resized_orig = image.scale(orig, resized_w, resized_h)
    local resized_mask = image.scale(mask, resized_w, resized_h)

    -- inpaint center coordinate in resized image
    local center_x, center_y = torch.floor(scale * (x0+x1)/2), torch.floor(scale * (y0+y1)/2)
    center_x = math.min(math.max(center_x, opt.fineSize/2), resized_w-opt.fineSize/2)
    center_y = math.min(math.max(center_y, opt.fineSize/2), resized_h-opt.fineSize/2)

    -- print(center_x, center_y,resized_w, resized_h)

    -- construct input to the inpainting net
    local centerImage = resized_orig[{{}, {center_y-opt.fineSize/2+1, center_y+opt.fineSize/2}, {center_x-opt.fineSize/2+1, center_x+opt.fineSize/2}}]:clone()
    local centerMask = resized_mask[{{center_y-opt.fineSize/2+1, center_y+opt.fineSize/2}, {center_x-opt.fineSize/2+1, center_x+opt.fineSize/2}}]:clone()
    -- print(centerMask, centerMask:sum())
    
    -- predict
    input_image_ctx[1]:copy(centerImage)
    local delta = net:forward(input_image_ctx):float()
    local pred_center = torch.add(centerImage, delta)
    pred_center[pred_center:gt(1.0)] = 1.0
    pred_center[pred_center:lt(-1.0)] = -1.0

    -- reconstruct origin image
    local resized_recon = resized_orig:clone()
    resized_recon[{{}, {center_y-opt.fineSize/2+1, center_y+opt.fineSize/2}, {center_x-opt.fineSize/2+1, center_x+opt.fineSize/2}}]:copy(pred_center[{{1},{},{},{}}])
    local orig_sized_recon = image.scale(resized_recon, imgW, imgH)
    local final_recon = orig_sized_recon:clone()

    -- re-transform pixel value to normal
    orig:add(1):mul(0.5)
    final_recon:add(1):mul(0.5)

    -- save
    pretty_output = torch.Tensor(2, opt.nc, imgH, imgW)
    pretty_output[1]:copy(orig)
    pretty_output[2]:copy(final_recon)
    local outdir = paths.concat(outbase, string.sub(paths.basename(opt.net), 1, -4) .. '-onlymask')
    if not paths.dirp(outdir) then paths.mkdir(outdir) end
    image.save(paths.concat(outdir, paths.basename(maskPath)), image.toDisplayTensor(pretty_output))
end

local maskDict = {}
if opt.maskDir~='' and not opt.wholeAsInput then
    print('inpaint only in the given mask region .......')
    for file in paths.iterfiles(opt.maskDir) do
        maskDict[file] = paths.concat(opt.maskDir, file) -- NOTE: mask filename is the same as image filename
    end
else
    print('inpaint in the whole image .......')
end

for file in paths.iterfiles(opt.inputDir) do
    local origPath = paths.concat(opt.inputDir, file)
    local maskPath = maskDict[file]
    predict(origPath, maskPath)
end

