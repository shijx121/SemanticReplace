require 'torch'
require 'nn'
require 'optim'
require 'image'
require 'models'
require 'nngraph'
require 'cunn'
require 'cudnn'
require 'detection'
json = require 'cjson'
disp = require 'display'
util = paths.dofile('util.lua')

opt = {
    cocoDir = '/data/mscoco',
    subCatsJson = './data/cocoObj/subCats.json',
    featNormJson = './data/cocoObj/featNorm.json',
    inpaintNetPath = './checkpoints/Segx256_no-overlap_no-aug-Glr/180_net_G.t7',
    featNetModelPath = './FRCNN.lua',
    featNetWeightPath = './models/frcnn_vgg16_mscoco_iter_100000.t7',
   batchSize = 1,         -- number of samples to produce
   fineSize = 256,         -- size of random crops. Only 128 or 256 supported.
   ngf = 64,               -- #  of gen filters in first conv layer
   ndf = 64,               -- #  of discrim filters in first conv layer
   nc = 3,                 -- # of channels in input
   nfeat = 128,            -- # of features
   nfeat_compressed = 3,   -- # of channels of compressed feature, which is obtained by 1x1 conv
   wRecon = 1,
   wAdv = 1e-3,
   wFeat = 1e-2,
   nThreads = 4,           -- #  of data loading threads to use
   niter = 500000,             -- #  of iter at starting learning rate
   lr = 0.002,            -- initial learning rate for adam
   lrd = 5e-4,             -- learning rate decay, make lr multiply 1/11 after 2e5 updates
   beta1 = 0.5,            -- momentum term of adam
   display_id = 10,        -- display window id.
   gpu = 1,                -- gpu=X is GPU mode on GPU X
   name = 'joint-modelx256',        -- name of the experiment you are running
   manualSeed = 123456789,         -- 0 means random seed
}
for k,v in pairs(opt) do opt[k] = tonumber(os.getenv(k)) or os.getenv(k) or opt[k] end
print(opt)
if opt.display == 0 then opt.display = false end
cutorch.setDevice(opt.gpu)


-- set seed
if opt.manualSeed == 0 then
    opt.manualSeed = torch.random(1, 10000)
end
print("Seed: " .. opt.manualSeed)
torch.manualSeed(opt.manualSeed)
torch.setnumthreads(1)
torch.setdefaulttensortype('torch.FloatTensor')

-- create data loader
local DataLoader = paths.dofile('data/cocoObj/data.lua')
local data = DataLoader.new(opt.nThreads, opt)
print("Dataset Size: ", data:size())

---------------------------------------------------------------------------
-- Initialize network variables
---------------------------------------------------------------------------
local function weights_init(m)
   local name = torch.type(m)
   if name:find('Convolution') then
      m.weight:normal(0.0, 0.02)
      m.bias:fill(0)
   elseif name:find('BatchNormalization') then
      if m.weight then m.weight:normal(1.0, 0.02) end
      if m.bias then m.bias:fill(0) end
   end
end

local real_label = 1
local fake_label = 0

---------------------------------------------------------------------------
-- load inpaint net
---------------------------------------------------------------------------
local netInpaint = torch.load(opt.inpaintNetPath)
netInpaint:apply(function(m) if m.accGradParameters then m.accGradParameters = function() end; m.gradWeight = nil; m.gradBias = nil; end end)
netInpaint:evaluate()
netInpaint:cuda()
print('inpaint net loaded.')

---------------------------------------------------------------------------
-- load feature net
---------------------------------------------------------------------------
local netFeat = dofile(opt.featNetModelPath)(opt) 
local loaded = torch.load(opt.featNetWeightPath)
local model_modules = netFeat:listModules()
local loaded_modules = loaded:listModules()
for i=1, #loaded_modules do
    local copy_name = loaded_modules[i].name
    if copy_name then
        for j=1, #model_modules do
            local my_name = model_modules[j].name
            if my_name and my_name == copy_name then
                print('Copying weights from ' .. my_name .. ' layer!')
                model_modules[j].weight:copy(loaded_modules[i].weight)
                model_modules[j].bias:copy(loaded_modules[i].bias)
            end
        end
    end
end

netFeat:evaluate()
netFeat:cuda()
print('feature net loaded.')

local _ = json.decode(io.open(opt.featNormJson, 'r'):read())
local feat_mean, feat_std = torch.Tensor(_.mean), torch.sqrt(torch.Tensor(_.var))

function featnorm(f)
    f = f:float()
    return torch.cdiv(f-feat_mean, feat_std)
end

function prep4feat(im)
    -- input: rgb, [-1,1], 3xhxw. Note im has no batch dimension!
    -- output: bgr, [0,255] and subtract mean
    local im_out = im:clone()
    im_out[1] = im[3]; im_out[3] = im[1];
    im_out:add(1):mul(0.5*255)
    im_out[1]:add(-102.9801)
    im_out[2]:add(-115.9465)
    im_out[3]:add(-122.7717)
    return im_out
end

function bp4feat(df)
    -- propogate gradient from output of prep4feat to input
    -- Note df has no batch dimension!
    local df_out = df:clone()
    df_out[1], df_out[3] = df[3], df[1]
    df_out:mul(0.5*255)
    return df_out
end

---------------------------------------------------------------------------
-- Generator/blend net
---------------------------------------------------------------------------
local netG
if opt.fineSize == 256 then
    netG = defineG_unet_256_FCfeat_residualblock(opt.nc+1, opt.nc, opt.ngf, opt.nfeat) -- TODO architecture
end
netG:apply(weights_init)
netG:cuda()

---------------------------------------------------------------------------
-- Adversarial discriminator net
---------------------------------------------------------------------------
local netD
if opt.fineSize == 256 then
    netD = defineD_256(opt.nc, opt.ndf)
end
netD:apply(weights_init)
netD:cuda()

print('NetG:',netG)
print('NetD:',netD)

local parametersD, gradParametersD = netD:getParameters()
local parametersG, gradParametersG = netG:getParameters()

---------------------------------------------------------------------------
-- Loss Metrics
---------------------------------------------------------------------------
local criterionAdv = nn.BCECriterion()
local criterionL1 = nn.SmoothL1Criterion()
criterionAdv:cuda();      
criterionL1:cuda();

---------------------------------------------------------------------------
-- Setup Solver
---------------------------------------------------------------------------
optimStateG = {
   learningRate = opt.lr,
   beta1 = opt.beta1,
   learningRateDecay = opt.lrd,
}
optimStateD = {
   learningRate = opt.lr,
   beta1 = opt.beta1,
   learningRateDecay = opt.lrd,
}

---------------------------------------------------------------------------
-- Initialize data variables
---------------------------------------------------------------------------
local label = torch.Tensor(opt.batchSize):cuda()
local input_of_inpaint = torch.Tensor(opt.batchSize, 3, opt.fineSize, opt.fineSize):cuda()
local input_of_G_img = torch.Tensor(opt.batchSize, 3+1, opt.fineSize, opt.fineSize):cuda()
local input_of_G_feat = torch.Tensor(opt.batchSize, opt.nfeat, 1, 1):cuda()
local input_of_D = torch.Tensor(opt.batchSize, 3, opt.fineSize, opt.fineSize):cuda()
local errD, errGAdv, errRecon, errFeat
local tm = torch.Timer()
local data_tm = torch.Timer()

local imA, maskA, clsA, x1A,y1A,x2A,y2A, imB, maskB, clsB, x1B,y1B,x2B,y2B, imF, maskF, x1F,y1F,x2F,y2F, im_holder
local _, h, w, h_factor, w_factor
local inpaint_out, featB, featFakeB, fake
local same=true  -- true means imB equals to imA and recon loss for whole. false means recon loss outside bbox.

---------------------------------------------------------------------------
-- Define generator and adversary closures
---------------------------------------------------------------------------
-- create closure to evaluate f(X) and df/dX of discriminator
local fDx = function(x)
   netD:apply(function(m) if torch.type(m):find('Convolution') then m.bias:zero() end end)
   netG:apply(function(m) if torch.type(m):find('Convolution') then m.bias:zero() end end)

   gradParametersD:zero()

   -- fetch data
   data_tm:reset(); data_tm:resume()
   imA, maskA, clsA, x1A, y1A, x2A, y2A = data:getBatch()
   if same then
      imB, maskB, clsB, x1B, y1B, x2B, y2B = imA, maskA, clsA, x1A, y1A, x2A, y2A
   else
      imB, maskB, clsB, x1B, y1B, x2B, y2B = data:getBatch()
   end
   assert(imA:size(2)==maskA:size(1) and imA:size(3)==maskA:size(2))
   data_tm:stop()

   -- resize imA to fineSize
   h, w = imA:size(2), imA:size(3)
   h_factor, w_factor = opt.fineSize/h, opt.fineSize/w
   x1F = math.ceil((x1A-1)*w_factor+1)
   x2F = math.ceil((x2A-1)*w_factor+1)
   y1F = math.ceil((y1A-1)*h_factor+1)
   y2F = math.ceil((y2A-1)*h_factor+1)
   x1F = math.min(x1F, opt.fineSize)
   x2F = math.min(x2F, opt.fineSize)
   y1F = math.min(y1F, opt.fineSize)
   y2F = math.min(y2F, opt.fineSize)
   imF = image.scale(imA, opt.fineSize, opt.fineSize, 'bicubic')
   maskF = image.scale(maskA, opt.fineSize, opt.fineSize, 'bicubic')


   -- input to the netInpaint: pollute object A in imA's Finesized
   im_holder = imF:clone()
   im_holder[{{1},{},{}}][maskF] = 2*117.0/255.0 - 1.0
   im_holder[{{2},{},{}}][maskF] = 2*104.0/255.0 - 1.0
   im_holder[{{3},{},{}}][maskF] = 2*123.0/255.0 - 1.0

   -- netInpaint forward: inpaint object A
   input_of_inpaint[1]:copy(im_holder)
   inpaint_out = netInpaint:forward(input_of_inpaint)

   -- input to the netFeat: prepare imB and object B's bbox
   im_holder = prep4feat(imB)
   im_holder = im_holder:view(1, imB:size(1), imB:size(2), imB:size(3)):cuda()
   bbox = torch.Tensor({{1, x1B, y1B, x2B, y2B}}):cuda()

   -- netFeat forward: get object B's feature
   featB = netFeat:forward({im_holder, bbox}):clone()

   ------------
   --construct input of G. NOTE: input of G must be normalized to avoid gradient vanishment
   ------------
   -- inpaint output
   input_of_G_img[{{},{1,3},{},{}}]:copy(inpaint_out)
   input_of_G_img[{{},{4},{},{}}]:fill(-1)
   input_of_G_img[{{},{4},{y1F,y2F},{x1F,x2F}}]=1
   -- feature of object B, only in the bbox x1F,y1F;x2F,y2F
   local featBnormed = featnorm(featB[1])
   input_of_G_feat:copy(featBnormed)
   --]]
   ------------
   
   ------------
   -- netG forward: get fake, blend object B at the position of object A
   -- fake = netG:forward({input_of_G_img, input_of_G_feat})
   _ = netG:forward({input_of_G_img, input_of_G_feat})
   fake = _[1]
   -- print(torch.squeeze(_[2]))
   ------------

   -- train with real
   label:fill(real_label)
   input_of_D[1]:copy(imF)
   local output = netD:forward(input_of_D)
   local errD_real = criterionAdv:forward(output, label)
   local df_do = criterionAdv:backward(output, label)
   netD:backward(input_of_D, df_do)
   
   -- train with fake
   label:fill(fake_label)
   input_of_D:copy(fake)
   local output = netD:forward(input_of_D)
   local errD_fake = criterionAdv:forward(output, label)
   local df_do = criterionAdv:backward(output, label)
   netD:backward(input_of_D, df_do)

   errD = errD_real + errD_fake

   return errD, gradParametersD
end

   local df_dg_total
-- create closure to evaluate f(X) and df/dX of generator
local fGx = function(x)
   netD:apply(function(m) if torch.type(m):find('Convolution') then m.bias:zero() end end)
   netG:apply(function(m) if torch.type(m):find('Convolution') then m.bias:zero() end end)

   gradParametersG:zero()

   local errG_total
   -------------
   -- adv loss
   -------------
   label:fill(real_label)
   local output = netD.output
   errGAdv = criterionAdv:forward(output, label)
   local df_do = criterionAdv:backward(output, label)
   local df_dg_adv = netD:updateGradInput(input_of_D, df_do)

   -------------
   -- recon loss outside bbox (x1F,y1F), (x2F,y2F)
   -------------
   local origin_holder = imF:clone():cuda():view(1, imF:size(1), imF:size(2), imF:size(3))
   local fake_holder = fake:clone()
   if not same then
      origin_holder[{{},{},{y1F,y2F},{x1F,x2F}}]=0
      fake_holder[{{},{},{y1F,y2F},{x1F,x2F}}]=0
   end
   errRecon = criterionL1:forward(fake_holder, origin_holder) 
   local df_dg_recon = criterionL1:backward(fake_holder, origin_holder):clone()
   
   -------------
   -- feature loss between bbox (x1F,y1F), (x2F,y2F) of fake and featB
   ------------
   -- input to the netFeat: prepare fake and inpainted object A's bbox
   im_holder = prep4feat(torch.squeeze(fake))
   im_holder = im_holder:view(1, 3, opt.fineSize, opt.fineSize):cuda()
   bbox = torch.Tensor({{1, x1F, y1F, x2F, y2F}}):cuda()
   -- netFeat forward: get fake object B's feature
   featFakeB = netFeat:forward({im_holder, bbox})
   -- propogate to featNet's output
   errFeat = criterionL1:forward(featFakeB, featB)
   local df_dfeat = criterionL1:backward(featFakeB, featB)
   -- propogate to featNet's input
   local df_dg_feat = netFeat:updateGradInput({im_holder, bbox}, df_dfeat)[1]
   -- propogate to netG's output
   df_dg_feat = bp4feat(torch.squeeze(df_dg_feat))
   df_dg_feat = df_dg_feat:view(1, 3, opt.fineSize, opt.fineSize):cuda()

   -------------
   -- total error and grad
   -------------
   errG_total = opt.wAdv*errGAdv + opt.wRecon*errRecon + opt.wFeat*errFeat
   df_dg_total = torch.mul(df_dg_adv, opt.wAdv):add(torch.mul(df_dg_recon, opt.wRecon)):add(torch.mul(df_dg_feat, opt.wFeat))
 
   -- netG:backward({input_of_G_img, input_of_G_feat}, df_dg_total)
   netG:backward({input_of_G_img, input_of_G_feat}, {df_dg_total,torch.Tensor(_[2]:float()):zero():cuda()})

    --[[
   fake = netG:forward({input_of_G_img, input_of_G_feat})
   im_holder = prep4feat(torch.squeeze(fake))
   im_holder = im_holder:view(1, 3, opt.fineSize, opt.fineSize):cuda()
   bbox = torch.Tensor({{1, x1F, y1F, x2F, y2F}}):cuda()
   featFakeB = netFeat:forward({im_holder, bbox})
   errFeat = criterionL1:forward(featFakeB, featB)
   print(errFeat)]]

   return errG_total, gradParametersG
end

---------------------------------------------------------------------------
-- Train Context Encoder
---------------------------------------------------------------------------
local plot_config = {
    title = opt.name,
    labels = {"iter", "100*err_recon", "err_feat", "err_GAdv", "err_D"},
    ylabel = "loss",
}
local plot_data = {}
for i = 1, opt.niter do
  tm:reset()
  -- randomly decide whether same=ture at this step
  same = math.random() > 0.5
  -- (1) Update D network: maximize log(D(x)) + log(1 - D(G(z)))
  optim.adam(fDx, parametersD, optimStateD)
  -- (2) Update G network: maximize log(D(G(z)))
  optim.adam(fGx, parametersG, optimStateG)

  -- display: [-1,1] to [0,255] and draw bbox
  if i % 10 == 0 then
      imF:add(1):mul(0.5*255); imF = image.drawRect(imF, x1F, y1F, x2F, y2F, {lineWidth=5,color={255,0,0}})
      disp.image(imF, {win=opt.display_id, title=opt.name .. '-A'})

      fake=fake:add(1):mul(0.5*255):float():squeeze()
      fake = image.drawRect(fake, x1F, y1F, x2F, y2F, {lineWidth=5,color={255,0,0}})
      disp.image(fake, {win=opt.display_id+1, title=opt.name .. '-fake'})

      inpaint_out=inpaint_out:add(1):mul(0.5*255):float():squeeze()
      inpaint_out = image.drawRect(inpaint_out, x1F, y1F, x2F, y2F, {lineWidth=5,color={255,0,0}})
      disp.image(inpaint_out, {win=opt.display_id+2, title=opt.name .. '-inpaint'})

      imB:add(1):mul(0.5*255); imB = image.drawRect(imB, x1B, y1B, x2B, y2B, {lineWidth=5,color={255,0,0}})
      disp.image(imB, {win=opt.display_id+3, title=opt.name .. '-B'})
      
      table.insert(plot_data, {i, 100*errRecon, errFeat, errGAdv, errD})
      plot_config.win = disp.plot(plot_data, plot_config) 
  end

  -- logging
  if i % 10 == 0 then
     collectgarbage()
     print(('step: [%7d/%7d] Time: %.3f  DataTime: %.3f\t'
               .. 'Err_Recon:%.4f  Err_Feat:%.4f  Err_GAdv:%.4f  Err_D:%.4f'):format(
             i, opt.niter, tm:time().real, data_tm:time().real, 
             errRecon or -1, errFeat or -1, errGAdv or -1, errD  or -1))
  end
   parametersD, gradParametersD = nil, nil -- nil them to avoid spiking memory
   parametersG, gradParametersG = nil, nil
   if i % 50000 == 0 then
       local outdir = paths.concat('checkpoints', opt.name)
      if not paths.dirp(outdir) then paths.mkdir(outdir) end
      torch.save('checkpoints/' .. opt.name .. '/' .. i .. '_net_G.t7', netG)  -- util.save will cause oom due to gmodule
      util.save('checkpoints/' .. opt.name .. '/' .. i .. '_net_D.t7', netD, opt.gpu)
      print('save checkpoint' .. i)
   end
   parametersD, gradParametersD = netD:getParameters() -- reflatten the params and get them
   parametersG, gradParametersG = netG:getParameters()
end
