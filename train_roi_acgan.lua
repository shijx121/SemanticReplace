require 'torch'
require 'nn'
require 'optim'
require 'image'
require 'models'
require 'nngraph'
require 'cunn'
require 'cudnn'
require 'detection'
json = require 'cjson'
disp = require 'display'
util = paths.dofile('util.lua')

opt = {
    cocoDir = '/data/mscoco',
    subCatsJson = './data/cocoObj/subCats.json',
   batchSize = 1,         -- number of samples to produce
   fineSize = 256,         -- size of random crops. Only 128 or 256 supported.
   ngf = 64,               -- #  of gen filters in first conv layer
   ndf = 64,               -- #  of discrim filters in first conv layer
   nc = 3,                 -- # of channels in input
   nclass = 20,
   wCls = 1,
   wAdv = 1,
   nThreads = 4,           -- #  of data loading threads to use
   niter = 500000,             -- #  of iter at starting learning rate
   lr = 0.00002,
   lrd = 5e-4,             -- learning rate decay, make lr multiply 1/11 after 2e5 updates
   beta1 = 0.5,            -- momentum term of adam
   display = 1,
   display_id = 50,        -- display window id.
   gpu = 1,                -- gpu=X is GPU mode on GPU X
   name = 'ROI-ACGAN',        -- name of the experiment you are running
   manualSeed = 1234,         -- 0 means random seed

   useNoise = 1,          -- 0 means not use noise
   noiseDim = 100,

   debug=false,
}
for k,v in pairs(opt) do opt[k] = tonumber(os.getenv(k)) or os.getenv(k) or opt[k] end
print(opt)
if opt.display == 0 then opt.display = false end
if opt.useNoise == 0 then opt.useNoise = false end
if not opt.useNoise then opt.noiseDim = 0 end
cutorch.setDevice(opt.gpu)

-- set seed
if opt.manualSeed == 0 then
    opt.manualSeed = torch.random(1, 10000)
end
print("Seed: " .. opt.manualSeed)
torch.manualSeed(opt.manualSeed)
torch.setnumthreads(1)
torch.setdefaulttensortype('torch.FloatTensor')

-- create data loader
local DataLoader = paths.dofile('data/cocoObj/data.lua')
local data = DataLoader.new(opt.nThreads, opt)
print("Dataset Size: ", data:size())

---------------------------------------------------------------------------
-- Initialize network variables
---------------------------------------------------------------------------
local function weights_init(m)
   local name = torch.type(m)
   if name:find('Convolution') then
      m.weight:normal(0.0, 0.02)
      m.bias:fill(0)
   elseif name:find('BatchNormalization') then
      if m.weight then m.weight:normal(1.0, 0.02) end
      if m.bias then m.bias:fill(0) end
   end
end

local real_label = 1
local fake_label = 0


---------------------------------------------------------------------------
-- Generator/blend net
---------------------------------------------------------------------------
local netG = defineG_ROI_ACGAN(opt.nc, opt.nc, opt.ngf, opt.nclass + opt.noiseDim, opt.debug)
netG:apply(weights_init)
netG:cuda()

---------------------------------------------------------------------------
-- Adversarial discriminator net
---------------------------------------------------------------------------
local netD = defineD_ROI_ACGAN(opt.nc, opt.ndf, opt.nclass)
netD:apply(weights_init)
netD:cuda()

print('NetG:',netG)
print('NetD:',netD)

local parametersD, gradParametersD = netD:getParameters()
local parametersG, gradParametersG = netG:getParameters()

---------------------------------------------------------------------------
-- Loss Metrics
---------------------------------------------------------------------------
local criterionAdv = nn.BCECriterion()
local criterionCls = nn.CrossEntropyCriterion()
local criterionL1 = nn.SmoothL1Criterion()
criterionCls:cuda();
criterionAdv:cuda();
criterionL1:cuda()

---------------------------------------------------------------------------
-- Setup Solver
---------------------------------------------------------------------------
optimStateG = {
   learningRate = opt.lr,
   beta1 = opt.beta1,
   learningRateDecay = opt.lrd,
}
optimStateD = {
   learningRate = opt.lr,
   beta1 = opt.beta1,
   learningRateDecay = opt.lrd,
}

---------------------------------------------------------------------------
-- Initialize data variables
---------------------------------------------------------------------------
local label = torch.Tensor(opt.batchSize):cuda()
local input_of_G_feat = torch.Tensor(opt.batchSize, opt.nclass+opt.noiseDim, 1, 1):cuda()
local input_of_D_img = torch.Tensor(opt.batchSize, 3, opt.fineSize, opt.fineSize):cuda()
local fake_holder = torch.Tensor(opt.batchSize, 3, opt.fineSize, opt.fineSize):cuda()
local zero_holder = torch.Tensor(opt.batchSize, 3, opt.fineSize, opt.fineSize):cuda():zero()
local bbox = torch.Tensor(1, 5):cuda()
local errD, errGAdv, errCls, errD_cls, errD_adv
local tm = torch.Timer()
local data_tm = torch.Timer()

local imA, maskA, clsA, x1A,y1A,x2A,y2A
local fake

---------------------------------------------------------------------------
-- Define generator and adversary closures
---------------------------------------------------------------------------
-- create closure to evaluate f(X) and df/dX of discriminator
local fDx = function(x)
   netD:apply(function(m) if torch.type(m):find('Convolution') then m.bias:zero() end end)
   netG:apply(function(m) if torch.type(m):find('Convolution') then m.bias:zero() end end)

   gradParametersD:zero()

   -- fetch data
   data_tm:reset(); data_tm:resume()
   imA, maskA, clsA, x1A, y1A, x2A, y2A = data:getBatch()
   assert(imA:size(2)==maskA:size(1) and imA:size(3)==maskA:size(2))
   local tmp = imA:clone()
   imA = imA:zero()
   imA[{{},{y1A,y2A},{x1A,x2A}}] = tmp[{{},{y1A,y2A},{x1A,x2A}}]  -- set pixels outside bbox to 0
   data_tm:stop()

   bbox:copy(torch.Tensor({{1, x1A, y1A, x2A, y2A}}))

   ------------
   --construct input of G
   ------------
   input_of_G_feat[{{},{1,opt.nclass}}]:zero()
   input_of_G_feat[{{},{clsA}}]=1
   if opt.useNoise then
       input_of_G_feat[{{},{opt.nclass+1,opt.nclass+opt.noiseDim}}]:normal(0, 1)
   end
   
   ------------
   -- netG forward: get fake
   if opt.debug then
       _ = netG:forward({input_of_G_feat, bbox})
       fake = _[1]
       print(torch.squeeze(_[3])[{{1,3}}])
   else
       fake = netG:forward({input_of_G_feat, bbox})
   end
   ------------

   -- train with real, netB has been runned for imB !
   label:fill(real_label)
   input_of_D_img:copy(imA)
   local output = netD:forward({input_of_D_img, bbox})
   local errD_cls_real = criterionCls:forward(output[1], clsA)
   local df_do_cls_real = criterionCls:backward(output[1], clsA):clone()
   local errD_real = criterionAdv:forward(output[2], label)
   local df_do_real = criterionAdv:backward(output[2], label)
   netD:backward({input_of_D_img, bbox}, {df_do_cls_real, df_do_real})
   
   -- train with fake
   label:fill(fake_label)
   input_of_D_img:copy(fake)
   local output = netD:forward({input_of_D_img, bbox})
   local errD_cls_fake = criterionCls:forward(output[1], clsA)
   local df_do_cls_fake = criterionCls:backward(output[1], clsA):clone()
   local errD_fake = criterionAdv:forward(output[2], label)
   local df_do_fake = criterionAdv:backward(output[2], label)
   netD:backward({input_of_D_img, bbox}, {df_do_cls_fake, df_do_fake})

   errD = errD_real + errD_fake + errD_cls_real + errD_cls_fake
   errD_adv = errD_real + errD_fake
   errD_cls = errD_cls_real + errD_cls_fake

   return errD, gradParametersD
end

-- create closure to evaluate f(X) and df/dX of generator
local fGx = function(x)
   netD:apply(function(m) if torch.type(m):find('Convolution') then m.bias:zero() end end)
   netG:apply(function(m) if torch.type(m):find('Convolution') then m.bias:zero() end end)

   gradParametersG:zero()

   local errG_total
   local df_dg_total
   -------------
   -- adv loss
   -------------
   label:fill(real_label)
   local output = netD.output
   errCls = criterionCls:forward(output[1], clsA)
   local df_do_cls = criterionCls:backward(output[1], clsA)
   errGAdv = criterionAdv:forward(output[2], label)
   local df_do = criterionAdv:backward(output[2], label)

   df_do_cls:mul(opt.wCls);
   df_do:mul(opt.wAdv);
   local df_dg_fromD = netD:updateGradInput({input_of_D_img, bbox}, {df_do_cls, df_do})[1]

   -------------
   -- recon loss outside bbox
   -------------
   fake_holder:copy(fake)
   fake_holder[{{},{},{y1A,y2A},{x1A,x2A}}]=0
   local errZero = criterionL1:forward(fake_holder, zero_holder)
   local dzero_dg = criterionL1:backward(fake_holder, zero_holder)

   -------------
   -- total error and grad
   -------------
   errG_total = opt.wAdv*errGAdv + opt.wCls*errCls + errZero
   df_dg_total = df_dg_fromD:add(dzero_dg) 
 
   if opt.debug then
      netG:backward({input_of_G_feat, bbox}, {df_dg_total, _[2]:clone():zero(), _[3]:clone():zero()})
   else
      netG:backward({input_of_G_feat, bbox}, df_dg_total)
   end

   return errG_total, gradParametersG
end

---------------------------------------------------------------------------
-- Train Context Encoder
---------------------------------------------------------------------------
local plot_config = {
    title = opt.name,
    labels = {"iter", "errG_cls", "errG_Adv", "errD_adv", "errD_cls"},
    ylabel = "loss",
}
local plot_data = {}
for i = 1, opt.niter do
  tm:reset(); tm:resume()
  -- (1) Update D network: maximize log(D(x)) + log(1 - D(G(z)))
  optim.adam(fDx, parametersD, optimStateD)
  -- (2) Update G network: maximize log(D(G(z)))
  optim.adam(fGx, parametersG, optimStateG)

  -- display: [-1,1] to [0,255] and draw bbox
  if i % 100 == 0 then
      imA:add(1):mul(0.5*255); imA = image.drawRect(imA, x1A, y1A, x2A, y2A, {lineWidth=5,color={255,0,0}})
      disp.image(imA, {win=opt.display_id, title=opt.name .. '-A'})

      fake=fake:add(1):mul(0.5*255):float():squeeze()
      fake = image.drawRect(fake, x1A, y1A, x2A, y2A, {lineWidth=5,color={255,0,0}})
      disp.image(fake, {win=opt.display_id+1, title=opt.name .. '-fake'})
  end

  -- logging
  if i % 20 == 0 then
     collectgarbage()
  tm:stop()
     print(('step: [%7d/%7d] Time: %.3f DataTime: %.3f\t'
               .. '  GCls:%.2f  GAdv:%.2f   DAdv:%.2f  DCls:%.2f'):format(
             i, opt.niter, tm:time().real, data_tm:time().real, 
              errCls or -1, errGAdv or -1, errD_adv  or -1, errD_cls or -1))
  end
   parametersD, gradParametersD = nil, nil -- nil them to avoid spiking memory
   parametersG, gradParametersG = nil, nil
   if i % 50000 == 0 then
       local outdir = paths.concat('checkpoints', opt.name)
      if not paths.dirp(outdir) then paths.mkdir(outdir) end
      torch.save('checkpoints/' .. opt.name .. '/' .. i .. '_net_G.t7', netG)
      torch.save('checkpoints/' .. opt.name .. '/' .. i .. '_net_D.t7', netD)
      print('save checkpoint' .. i)
   end
   parametersD, gradParametersD = netD:getParameters() -- reflatten the params and get them
   parametersG, gradParametersG = netG:getParameters()
end
