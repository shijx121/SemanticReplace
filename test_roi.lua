require 'torch'
require 'nn'
require 'optim'
require 'image'
require 'models'
require 'nngraph'
require 'cunn'
require 'cudnn'
require 'detection'
json = require 'cjson'
disp = require 'display'
util = paths.dofile('util.lua')

opt = {
    cocoDir = '/data/mscoco',
    subCatsJson = './data/cocoObj/subCats.json',
    featNormJson = './data/cocoObj/featNorm.json',
    inpaintNetPath = './checkpoints/Segx256_no-overlap_no-aug-Glr/300_net_G.t7',
    frcnnModelPath = './FRCNN.lua',
    frcnnWeightPath = './checkpoints/frcnn_vgg16_mscoco_iter_200000_class_10.t7',
    generator = './checkpoints/ROI-replace_lr002_deeperfusion_focus2.5/200000_net_G.t7',
   batchSize = 1,         -- number of samples to produce
   fineSize = 256,         -- size of random crops. Only 256 supported.
   nfeat = 128,            -- # of features
   nclass = 10,
   nThreads = 4,           -- #  of data loading threads to use
   display_id = 50,        -- display window id.
   gpu = 1,                -- gpu=X is GPU mode on GPU X
   sameProb = 0.5,
}
for k,v in pairs(opt) do opt[k] = tonumber(os.getenv(k)) or os.getenv(k) or opt[k] end
print(opt)
cutorch.setDevice(opt.gpu)

torch.setnumthreads(1)
torch.setdefaulttensortype('torch.FloatTensor')

-- create data loader
local DataLoader = paths.dofile('data/cocoObj/data.lua')
local data = DataLoader.new(opt.nThreads, opt)
print("Dataset Size: ", data:size())


---------------------------------------------------------------------------
-- load inpaint net
---------------------------------------------------------------------------
local netInpaint = torch.load(opt.inpaintNetPath)
netInpaint:apply(function(m) if m.accGradParameters then m.accGradParameters = function() end; m.gradWeight = nil; m.gradBias = nil; end end)
netInpaint:evaluate()
netInpaint:cuda()
print('inpaint net loaded.')

---------------------------------------------------------------------------
-- load weight from pretrained model 
---------------------------------------------------------------------------
function loadWeight(net, loaded)
    local model_modules = net:listModules()
    local loaded_modules = loaded:listModules()
    for i=1, #loaded_modules do
        local copy_name = loaded_modules[i].name
        if copy_name then
            for j=1, #model_modules do
                local my_name = model_modules[j].name
                if my_name and my_name == copy_name then
                    print('Copying weights from ' .. my_name .. ' layer!')
                    model_modules[j].weight:copy(loaded_modules[i].weight)
                    model_modules[j].bias:copy(loaded_modules[i].bias)
                end
            end
        end
    end
end

---------------------------------------------------------------------------
-- domain adaption 
---------------------------------------------------------------------------
function prep(im)
    -- input: rgb, [-1,1], 3xhxw. Note im has no batch dimension!
    -- output: bgr, [0,255] and subtract mean
    local im = torch.squeeze(im)
    local im_out = im:clone()
    im_out[1] = im[3]; im_out[3] = im[1];
    im_out:add(1):mul(0.5*255)
    im_out[1]:add(-102.9801)
    im_out[2]:add(-115.9465)
    im_out[3]:add(-122.7717)
    return im_out
end

---------------------------------------------------------------------------
-- Generator/blend net
---------------------------------------------------------------------------
local netG
netG = torch.load(opt.generator) 
netG:evaluate()
netG:cuda()

---------------------------------------------------------------------------
-- feature net
---------------------------------------------------------------------------
local netF = dofile(opt.frcnnModelPath)(opt)
local loaded = torch.load(opt.frcnnWeightPath)
loadWeight(netF, loaded)
loaded = nil
netF:evaluate()
collectgarbage()
print('frcnn netF loaded')

print('NetG:',netG)
print('netF:',netF)

---------------------------------------------------------------------------
-- Initialize data variables
---------------------------------------------------------------------------
local input_of_inpaint = torch.Tensor(opt.batchSize, 3, opt.fineSize, opt.fineSize):cuda()
local input_of_G_img = torch.Tensor(opt.batchSize, 3, opt.fineSize, opt.fineSize):cuda()
local input_of_F_img = torch.Tensor(opt.batchSize, 3, opt.fineSize, opt.fineSize):cuda()
local featA = torch.Tensor(opt.batchSize, 512, 16, 16):cuda()
local featB = torch.Tensor(opt.batchSize, 512, 16, 16):cuda()
local bboxA = torch.Tensor(1, 5):cuda()
local bboxB = torch.Tensor(1, 5):cuda()

local imA, maskA, clsA, x1A,y1A,x2A,y2A, imB, maskB, clsB, x1B,y1B,x2B,y2B, im_holder, _
local inpaint_out, semantic_featB, fake
local same=true  -- true means imB equals to imA and recon loss for whole. false means recon loss outside bbox.


function sample()
   -- fetch data
   imA, maskA, clsA, x1A, y1A, x2A, y2A = data:getBatch()
   if same then
      imB, maskB, clsB, x1B, y1B, x2B, y2B = imA, maskA, clsA, x1A, y1A, x2A, y2A
   else
      imB, maskB, clsB, x1B, y1B, x2B, y2B = data:getBatch()
   end
   bboxA:copy(torch.Tensor({{1,x1A,y1A,x2A,y2A}}))
   bboxB:copy(torch.Tensor({{1,x1B,y1B,x2B,y2B}}))

   -- input to the netInpaint: pollute object A in imA
   im_holder = imA:clone()
   im_holder[{{1},{},{}}][maskA] = 2*117.0/255.0 - 1.0
   im_holder[{{2},{},{}}][maskA] = 2*104.0/255.0 - 1.0
   im_holder[{{3},{},{}}][maskA] = 2*123.0/255.0 - 1.0

   -- netInpaint forward: inpaint object A
   input_of_inpaint:copy(im_holder)
   inpaint_out = netInpaint:forward(input_of_inpaint)

   -- prepare imB and get featB
   im_holder = prep(imB)
   input_of_F_img:copy(im_holder)
   _ = netF:forward({input_of_F_img, bboxB})
   featB:copy(_[3]) -- 512 x 16 x 16

    -- prepare inpainted imA and get featA (background) 
   im_holder = prep(inpaint_out)
   input_of_F_img:copy(im_holder)
   _ = netF:forward({input_of_F_img, bboxA})
   featA:copy(_[3]) -- 512 x 16 x 16

   ------------
   --construct input of G. NOTE: input of G must be normalized to avoid gradient vanishment TODO
   ------------
   -- inpaint output
   input_of_G_img:copy(inpaint_out)
   featB:div(128)
   featA:div(128)
   ------------
   
   ------------
   -- netG forward: get fake, blend object B at the position of bbox A
   fake = netG:forward({input_of_G_img, featA, bboxA, featB, bboxB})
   ------------
end


while true do
    same = math.random() <= opt.sameProb 
    sample()
    imA:add(1):mul(0.5*255); imA = image.drawRect(imA, x1A, y1A, x2A, y2A, {lineWidth=5,color={255,0,0}})
    disp.image(imA, {win=opt.display_id, title='-A'})

    fake=fake:add(1):mul(0.5*255):float():squeeze()
    fake = image.drawRect(fake, x1A, y1A, x2A, y2A, {lineWidth=5,color={255,0,0}})
    disp.image(fake, {win=opt.display_id+1, title='-fake'})

    inpaint_out=inpaint_out:add(1):mul(0.5*255):float():squeeze()
    inpaint_out = image.drawRect(inpaint_out, x1A, y1A, x2A, y2A, {lineWidth=5,color={255,0,0}})
    disp.image(inpaint_out, {win=opt.display_id+2, title='-inpaint'})

    imB:add(1):mul(0.5*255); imB = image.drawRect(imB, x1B, y1B, x2B, y2B, {lineWidth=5,color={255,0,0}})
    disp.image(imB, {win=opt.display_id+3, title='-B'})

    os.execute('sleep ' .. 2)
end
