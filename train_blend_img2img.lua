require 'torch'
require 'nn'
require 'optim'
require 'image'
require 'models'
util = paths.dofile('util.lua')

opt = {
    srcPath = '../image/blend-train/src/',
    dstPath = '../image/blend-train/dst/',
   batchSize = 64,         -- number of samples to produce
   fineSize = 128,         -- size of random crops. Only 64 and 128 supported.
   ngf = 64,               -- #  of gen filters in first conv layer
   ndf = 64,               -- #  of discrim filters in first conv layer
   nc = 3,                 -- # of channels in input
   kernel_size = 4,        -- kernel size of G and D
   nThreads = 4,           -- #  of data loading threads to use
   niter = 200,             -- #  of iter at starting learning rate
   ntrain = 1000,          -- max iteration times in one single epoch
   lr = 0.002,            -- initial learning rate for adam
   lrd = 5e-4,             -- learning rate decay, make lr decrease 10 times after 2e4 updates
   beta1 = 0.5,            -- momentum term of adam
   wRecon = 1,             -- weight of reconstruction
   wAdv = 0,         -- weight of adversarial loss
   display = 1,            -- display samples while training. 0 = false
   display_id = 30,        -- display window id.
   display_iter = 50,      -- # number of iterations after which display is updated
   gpu = 2,                -- gpu = 0 is CPU mode. gpu=X is GPU mode on GPU X
   name = 'blend_img2img',        -- name of the experiment you are running
   manualSeed = 0,         -- 0 means random seed
}
for k,v in pairs(opt) do opt[k] = tonumber(os.getenv(k)) or os.getenv(k) or opt[k] end
print(opt)
if opt.display == 0 then opt.display = false end

-- set seed
if opt.manualSeed == 0 then
    opt.manualSeed = torch.random(1, 10000)
end
print("Seed: " .. opt.manualSeed)
torch.manualSeed(opt.manualSeed)
torch.setnumthreads(1)
torch.setdefaulttensortype('torch.FloatTensor')

-- create data loader
local DataLoader = paths.dofile('data/blend_img2img/data.lua')
local data = DataLoader.new(opt.nThreads, opt)
print("Dataset Size: ", data:size())

---------------------------------------------------------------------------
-- Initialize network variables
---------------------------------------------------------------------------
local function weights_init(m)
   local name = torch.type(m)
   if name:find('Convolution') then
      m.weight:normal(0.0, 0.02)
      m.bias:fill(0)
   elseif name:find('BatchNormalization') then
      if m.weight then m.weight:normal(1.0, 0.02) end
      if m.bias then m.bias:fill(0) end
   end
end

local real_label = 1
local fake_label = 0

---------------------------------------------------------------------------
-- Generator net
---------------------------------------------------------------------------
local netG = defineG_unet_128(opt.nc, opt.nc, opt.ngf, opt.kernel_size)
netG:apply(weights_init)

---------------------------------------------------------------------------
-- Adversarial discriminator net
---------------------------------------------------------------------------
local netD = defineD_128(opt.nc, opt.ndf, opt.kernel_size)
netD:apply(weights_init)

---------------------------------------------------------------------------
-- Loss Metrics
---------------------------------------------------------------------------
local criterion = nn.BCECriterion()
local criterionL1 = nn.SmoothL1Criterion()

---------------------------------------------------------------------------
-- Setup Solver
---------------------------------------------------------------------------
optimStateG = {
   learningRate = opt.lr,
   beta1 = opt.beta1,
   learningRateDecay = opt.lrd,
}
optimStateD = {
   learningRate = opt.lr,
   beta1 = opt.beta1,
   learningRateDecay = opt.lrd,
}

---------------------------------------------------------------------------
-- Initialize data variables
---------------------------------------------------------------------------
local src, dst, delta, origin_fake, clip_fake, delta_float
local input_of_G = torch.Tensor(opt.batchSize, opt.nc, opt.fineSize, opt.fineSize)
local input_of_D = torch.Tensor(opt.batchSize, opt.nc, opt.fineSize, opt.fineSize)
local dst_cuda = torch.Tensor(opt.batchSize, opt.nc, opt.fineSize, opt.fineSize)
local label = torch.Tensor(opt.batchSize)

local errD, errG, errRecon
local epoch_tm = torch.Timer()
local tm = torch.Timer()
local data_tm = torch.Timer()

if pcall(require, 'cudnn') and pcall(require, 'cunn') and opt.gpu>0 then
    print('Using CUDNN !')
end
if opt.gpu > 0 then
   require 'cunn'
   cutorch.setDevice(opt.gpu)
   input_of_G = input_of_G:cuda()
   input_of_D = input_of_D:cuda()
   dst_cuda = dst_cuda:cuda()
   label = label:cuda()
   netD:cuda();           netG:cuda();           criterion:cuda();      
   criterionL1:cuda(); 
end
print('NetG:',netG)
print('NetD:',netD)

local parametersD, gradParametersD = netD:getParameters()
local parametersG, gradParametersG = netG:getParameters()

if opt.display then disp = require 'display' end

---------------------------------------------------------------------------
-- Define generator and adversary closures
---------------------------------------------------------------------------
-- create closure to evaluate f(X) and df/dX of discriminator
local fDx = function(x)
   netD:apply(function(m) if torch.type(m):find('Convolution') then m.bias:zero() end end)
   netG:apply(function(m) if torch.type(m):find('Convolution') then m.bias:zero() end end)

   gradParametersD:zero()

   -- train with real
   data_tm:reset(); data_tm:resume()
   src, dst = data:getBatch()
   input_of_G:copy(src)
   data_tm:stop()

   input_of_D:copy(dst)
   label:fill(real_label)
   local output = netD:forward(input_of_D)
   local errD_real = criterion:forward(output, label)
   local df_do = criterion:backward(output, label)
   netD:backward(input_of_D, df_do)
   
   -- train with fake
   delta = netG:forward(input_of_G)
   delta_float = delta:clone():float()
   origin_fake = torch.add(src, delta_float) 
   clip_fake = origin_fake:clone()
   clip_fake[origin_fake:gt(1.0)] = 1.0
   clip_fake[origin_fake:lt(-1.0)] = -1.0

   input_of_D:copy(clip_fake)
   label:fill(fake_label)
   local output = netD:forward(input_of_D)
   local errD_fake = criterion:forward(output, label)
   local df_do = criterion:backward(output, label)
   netD:backward(input_of_D, df_do)

   errD = errD_real + errD_fake

   return errD, gradParametersD
end

-- create closure to evaluate f(X) and df/dX of generator
local fGx = function(x)
   netD:apply(function(m) if torch.type(m):find('Convolution') then m.bias:zero() end end)
   netG:apply(function(m) if torch.type(m):find('Convolution') then m.bias:zero() end end)

   gradParametersG:zero()

   --[[ the following lines below were already executed in fDx, so save computation
    delta = netG:forward(input_of_G)
    ......
   local clip_fake = 
   input_of_D:copy(clip_fake)]]--
   label:fill(real_label) -- fake labels are real for generator cost

   local output = netD.output -- netD:forward(input_of_D) was already executed in fDx, so save computation
   errG = criterion:forward(output, label)
   local df_do = criterion:backward(output, label)
   local df_dg = netD:updateGradInput(input_of_D, df_do)

   dst_cuda:copy(dst)
   errRecon = criterionL1:forward(input_of_D, dst_cuda)
   local df_dg_recon = criterionL1:backward(input_of_D, dst_cuda)

   df_dg:mul(opt.wAdv):add(opt.wRecon,df_dg_recon)
   local errG_total = opt.wAdv*errG + opt.wRecon*errRecon
   netG:backward(input_of_G, df_dg)

   return errG_total, gradParametersG
end

---------------------------------------------------------------------------
-- Train Context Encoder
---------------------------------------------------------------------------
local plot_config = {
    title = "training loss over time",
    labels = {"epoch", "100*err_recon", "err_G", "err_D"},
    ylabel = "loss",
}
local plot_data = {}
local nstep = math.min(math.floor(data:size()/opt.batchSize), opt.ntrain)
for epoch = 1, opt.niter do
   epoch_tm:reset()
   for i = 1, nstep do
      tm:reset()
      -- (1) Update D network: maximize log(D(x)) + log(1 - D(G(z)))
      optim.adam(fDx, parametersD, optimStateD)

      -- (2) Update G network: maximize log(D(G(z)))
      optim.adam(fGx, parametersG, optimStateG)

      -- display
      if i % opt.display_iter == 0 and opt.display then
          disp.image(src, {win=opt.display_id+1, title=opt.name .. '-src'})
          delta_float:abs():mul(-1):add(1)  -- subtract delta_float from a white canvas.
          disp.image(delta_float, {win=opt.display_id+2, title=opt.name .. '-delta'})
          disp.image(clip_fake, {win=opt.display_id+3, title=opt.name .. '-prediction'})
          table.insert(plot_data, {(epoch-1)*nstep+i, 100*errRecon, errG, errD})
          plot_config.win = disp.plot(plot_data, plot_config)
      end

      -- logging
      if i % 10 == 0 then
         print(('Epoch: [%d][%8d / %8d]\t Time: %.3f  DataTime: %.3f  '
                   .. '  Err_Recon: %.4f   Err_G: %.4f  Err_D: %.4f'):format(
                 epoch, i, nstep,
                 tm:time().real, data_tm:time().real, errRecon or -1,
                 errG and errG or -1, errD and errD or -1))
      end
   end
   paths.mkdir('checkpoints')
   parametersD, gradParametersD = nil, nil -- nil them to avoid spiking memory
   parametersG, gradParametersG = nil, nil
   if epoch % 20 == 0 then
       local outdir = paths.concat('checkpoints', opt.name)
      if not paths.dirp(outdir) then paths.mkdir(outdir) end
      util.save('checkpoints/' .. opt.name .. '/' .. epoch .. '_net_G.t7', netG, opt.gpu)
      util.save('checkpoints/' .. opt.name .. '/' .. epoch .. '_net_D.t7', netD, opt.gpu)
   end
   parametersD, gradParametersD = netD:getParameters() -- reflatten the params and get them
   parametersG, gradParametersG = netG:getParameters()
   print(('End of epoch %d / %d \t Time Taken: %.3f'):format(
            epoch, opt.niter, epoch_tm:time().real))
end
